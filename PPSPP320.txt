//PPSPP320 JOB (SYS000),'WILL-S029',CLASS=F,NOTIFY=APCDBM,MSGCLASS=F,   00001000
//            MSGLEVEL=(1,1),REGION=0M                                  00002001
//**********************************************************************00003000
//*   NOTE:  THIS JOB CAN BE RUN TO RESET THE LAST_PGM_RUN IN          *00004000
//*          THE THF DATABASE, SHOULD A RESTART BE NECESSARY.          *00005000
//*                                                                    *00006000
//*       1) YOU MUST EDIT THE LAST_PGM_RUN AND PAY_PROCESS_ID         *00007000
//*          VALUES TO WHATEVER IS APPROPRIATE                         *00008000
//*       2) MAKE SURE IT IS POINTING TO CORRECT DATABASE              *00009000
//*             IE.  PPPDBC1.PPPV435A_PCR                              *00010000
//*                       VERSUS                                       *00020000
//*                  PPPDBB1.PPPV435A_PCR                              *00030000
//**********************************************************************00040000
//*                                                                     00050000
//JOBLIB  DD  DISP=SHR,                                                 00060000
//            DSN=DB2P.DSNLOAD                                          00070000
//EXECTSO  EXEC PGM=IKJEFT01,DYNAMNBR=20                                00080000
//DBRMLIB  DD DISP=SHR,                                                 00090000
//            DSN=DB2P.DBRMLIB.DATA                                     00100000
//SYSTSPRT DD SYSOUT=*                                                  00110000
//SYSPRINT DD SYSOUT=*                                                  00120000
//SYSUDUMP DD SYSOUT=*                                                  00130000
//SYSPUNCH DD SYSOUT=*                                                  00140000
//SYSTSIN  DD *                                                         00150000
 DSN SYSTEM(DB2P)                                                       00160000
 RUN  PROGRAM(DSNTIAD) PLAN(DSNTIA31) -                                 00170000
      LIB('DB2P.RUNLIB.LOAD')                                           00180000
 END                                                                    00190000
//* SQL STATEMENTS IN SYSIN DD                                          00200000
//SYSIN    DD *                                                         00210000
    SET CURRENT SQLID = 'APCDB2';                                       00220000
    UPDATE PPPDBC1.PPPV435A_PCR                                         00230000
    SET LAST_PGM_RUN = '34'                                             00240000
    WHERE PAY_PROCESS_ID = 'PPB39307';                                  00241000
//*                                                                     00250000
