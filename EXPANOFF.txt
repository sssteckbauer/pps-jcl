//EXPANOFF JOB SYS000,'RENA- S029',CLASS=F,MSGLEVEL=(1,1),
//             MSGCLASS=X,NOTIFY=DEVRFW,REGION=4096K,
//         COND=((8,LT),(5,EQ),(6,EQ),(7,EQ))
//JOBLIB    DD DSN=PPSP.LOADLIB,                                        00001000
//             DISP=SHR                                                 00001100
//          DD DSN=DB2P.DCA.LOAD,                                       00001200
//             DISP=SHR                                                 00001300
//          DD DSN=DB2P.DSNEXIT,                                        00001400
//             DISP=SHR                                                 00001500
//          DD DSN=DB2P.DSNLOAD,                                        00001600
//             DISP=SHR                                                 00001700
//** * * * * * * * * * * * * * * * * * * * * * * * * * * *              00002000
/*ROUTE PRINT RMT1                                                      00002100
/*JOBPARM PROCLIB=PROC06                                                00003700
//*
//****************************************************************/     00040000
//*   UNLOADS UC0ASC TABLE AND RELOADS IT WITH                   */     00050000
//*   PPST.MERITDEL.UC0ASC TO SHUT OFF ONLINE ACCESS TO CAMPUS   */     00060000
//****************************************************************/     00070000
//*                                                                     00080000
//UNCAT2   EXEC PGM=IEFBR14                                             00181000
//DELETE00  DD DSN=PPSP.PCD.UC0ASC.OUTPUT.JD95199,                      00182000
//             DISP=(MOD,DELETE),                                       00183000
//             UNIT=SYSDA,SPACE=(TRK,(0))                               00184000
//DELETE01  DD DSN=PPSP.PCD.LOADST.OUTPUT.JD95199,                      00182000
//             DISP=(MOD,DELETE),                                       00183000
//             UNIT=SYSDA,SPACE=(TRK,(0))                               00184000
//UNLOAD EXEC PGM=IKJEFT01,DYNAMNBR=20                                  00195500
//* >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>    00195600
//*                                                                     00195700
//*   STEP  UNLOAD   DB2 TABLE                                          00195800
//*                                                                     00195900
//* <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<    00196000
//SYSREC00  DD  DSN=PPSP.PCD.UC0ASC.OUTPUT.JD95199,                     00196100
//             DISP=(,CATLG,DELETE),                                    00196200
//             VOL=SER=APC002,
//             UNIT=SYSDA,SPACE=(1024,(9000,50),RLSE)                   00196300
//*            DCB=(LRECL=1000,BLKSIZE=23000,RECFM=VB)                  00196500
//SYSPUNCH  DD  DSN=PPSP.PCD.LOADST.OUTPUT.JD95199,                     00690000
//             DISP=(,CATLG,DELETE),                                    00700000
//             VOL=SER=APC002,
//             UNIT=SYSDA,SPACE=(800,(5000,50),RLSE)                    00710000
//*            DCB=(LRECL=1000,BLKSIZE=23000,RECFM=VB)                  00730000
//SYSPRINT DD  SYSOUT=*                                                 00740000
//SYSTSPRT DD  SYSOUT=*                                                 00750000
//SYSUDUMP DD  SYSOUT=*                                                 00760000
//*                                                                     00770000
//SYSTSIN DD *                                                          00108300
 DSN SYSTEM(DB2P)
 RUN PROGRAM(DSUTIAUL) PLAN(DSUTIB31) -
 LIB('DB2P.RUNLIB.LOAD')
//SYSIN     DD *                                                        00108400
  PPPDBC1.UC0ASC
/*                                                                      00108500
//*******************************************************************
//*   LOAD SHUT OFF VERSION OF UC0ASC TABLE TO C1 EDB
//*   TO SHUT OFF ONLINE ACCESS TO GENERAL CAMPUS
//*******************************************************************
//LOADPCD  EXEC DSNUPROC,
//             SYSTEM=DB2P,
//             UID='PPPPCD',
//             UTPROC=''
//*
//DSNUPROC.SORTWK01 DD DSN=PPSP.SORTWK01,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//DSNUPROC.SORTWK02 DD DSN=PPSP.SORTWK02,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//DSNUPROC.SORTWK03 DD DSN=PPSP.SORTWK03,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//DSNUPROC.SORTWK04 DD DSN=PPSP.SORTWK04,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SYSUT1: TEMP DATA SET TO HOLD SORTED KEYS IN
//*         "LOAD" AGAINST TABLES WITH INDEXES
//**********************************************
//DSNUPROC.SYSUT1 DD DSN=PPSP.SYSUT1,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SORTOUT: DATA SET TO HOLD SORTED KEYS FOR
//*          POSSIBLE UTILITY RESTART
//**********************************************
//DSNUPROC.SORTOUT DD DSN=PPSP.SORTOUT,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SYSERR: CONTAINS INFORMATION ON ERRORS
//*         ENCOUNTERED DURING UTILITY PROCESSING
//**********************************************
//DSNUPROC.SYSERR DD DSN=PPSP.SYSERR,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SYSMAP: CONTAINS INFORMATION ABOUT WHERE
//*         INPUT RECORDS ARE LOADED
//**********************************************
//DSNUPROC.SYSMAP DD DSN=PPSP.SYSMAP,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SYSDISC: CONTAINS ANY INPUT RECORDS WHICH
//*          WERE DISCARDED DURING THE "LOAD"
//**********************************************
//DSNUPROC.SYSDISC DD DSN=PPSP.SYSDISC,
//             DISP=(MOD,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SYSREC: THIS IS THE "LOAD" INPUT DATA SET
//**********************************************
//SYSREC00 DD DSN=PPST.MERITDEL.UC0ASC,DISP=SHR
//**********************************************
//* SYSIN: CONTAINS INPUT UTILITY CONTROL
//*         STATEMENTS
//**********************************************
//DSNUPROC.SYSIN  DD DSN=PPSP.PCD.LOADST.OUTPUT.JD95199,DISP=SHR
//*
