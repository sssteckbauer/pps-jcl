//$FTP1726 JOB (SYS000),'BLO - PRDCTL',NOTIFY=APCBLO,                   00010001
//*    RESTART=STEP2,
//             CLASS=K,MSGCLASS=F,MSGLEVEL=(1,1),REGION=0M
//PSFPRNT  OUTPUT DEFAULT=YES,JESDS=ALL
//*******************************************************************
//* NAME SERVER USED TO REPLACE I/P ADDRESS OF 128.48.141.6 (3/30/99)
//*
//*    ( DGIPNL SHOULD ALWAYS BE 'BINARY')
//*    ( UDB*   SHOULD ALWAYS BE 'EBCDIC'
//*                              'MODE B')
//*******************************************************************
//STEP1 EXEC PGM=FTP
//INPUT DD *
uccmvsb.ucop.edu
PAYFTP GETREL98
CWD 'PAYDIST.R1726'
LCD 'PPSTH.REL1726'
EBCDIC
MGET COBOL(*)    (REPLACE
MGET JCL(*)      (REPLACE
MGET REPORTS(*)  (REPLACE
QUIT
//OUTPUT DD DSN=PPSP.FTP.DATA(FTP$1726),DISP=SHR
//SYSPRINT DD SYSOUT=*
