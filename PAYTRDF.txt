//PAYTRDF  JOB SYS000,'APCDBM - S003',CLASS=A,MSGLEVEL=(1,1),           00010003
//             MSGCLASS=X,NOTIFY=APCDBM,REGION=2048K                    00020003
/*ROUTE PRINT RMT1                                                      00030000
//DELVSAM  EXEC PGM=IDCAMS                                              00040000
//SYSPRINT DD SYSOUT=*                                                  00050000
//SYSIN    DD *                                                         00060000
       DELETE (PPSP.PCICSV.PAYCORTR) -                                  00080000
          CLUSTER                                                       00090000
       DEFINE -                                                         00100000
         CLUSTER( -                                                     00110000
         NAME(PPSP.PCICSV.PAYCORTR) -                                   00120000
         VOL(PRDATA) -                                                  00130000
         KEYS(5,1) -                                                    00140000
         RECSZ(102 102) -                                               00150000
         FREESPACE(0,0) -                                               00160000
         IMBED -                                                        00170000
         SHAREOPTIONS(2) -                                              00180000
         UNIQUE -                                                       00190000
         INDEXED -                                                      00200000
         ) -                                                            00210000
       DATA( -                                                          00220000
         NAME(PPSP.PCICSV.PAYCORTR.DATA) -                              00230000
         CISZ(4096) -                                                   00240000
         CYL(1,0) -                                                     00250000
         ) -                                                            00260000
       INDEX( -                                                         00270000
         NAME(PPSP.PCICSV.PAYCORTR.INDX) -                              00280000
         )                                                              00290000
