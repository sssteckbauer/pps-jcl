//PPSPA11N JOB SYS000,'BEV - PRDCTL',MSGLEVEL=(1,1),                    00000100
//        NOTIFY=APCDBM,CLASS=F,REGION=20M,                             00000206
//*====================================================                 00110000
//*       RESTART=(UNCAT1),                                             00000407
//*====================================================                 00110000
//        MSGCLASS=F                                                    00000300
//*       COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                            00000500
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 2987    NEW MEDTRAN  FILES ONLY!!
//*++++++++++++++++++++++++++
//*====================================================                 00110000
//*  5/6/99 - AS OF REL1230, ACCEPTABLE CONDITION CODES
//*     FOR THIS JOB ARE 00 - 07
//*     *** 08 AND GREATER ARE NOT ACCEPTABLE ***
//*====================================================                 00110000
//* !!!!!!!!!! SEE IMPORTANT SCHEDULING NOTE BELOW  !!!!!!!!!
//*                   ON LINE #40
//************************************************************
//*
//JOBLIB   DD DSN=PPSP.LOADLIB,
//            DISP=SHR
//         DD DSN=DB2P.DCA.LOAD,
//            DISP=SHR
//         DD DSN=DB2P.DSNEXIT,
//            DISP=SHR
//         DD DSN=DB2P.DSNLOAD,
//            DISP=SHR
//*====================================================                 00110000
//* PPSP.HOSPTRAN.NEW   FILE ALLOCATION FOR BIWEEKLY PAYROLL
//*====================================================                 00110000
//* THIS JOB SHOULD BE RUN THE EVENING BEFORE ANY BIWEEKLY PAYROLL
//* AND AFTER DAILY PROCESSING.
//*
//*
//*
//* IMPORTANT:  IF A BACK-TO-BACK PAYROLL PROCESS IS SCHEDULED,
//*             (I.E. 'MO' ON WEDNESDAY AND 'BW' ON THURSDAY),
//*             THIS JOB SHOULD RUN AFTER THE SUCCESSFUL COMPLETION
//*             OF PPSPP910 FOR THE FIRST PAYROLL (I.E. 'MO' ).
//*
//* ||||||||||||||| S E E   N O T E  B E L O W  ||||||||||||||||||
//* VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV
//* **NO NEED....THE FILES HAVE BEEN TAKEN OUT OF THE 'PURGE' JOBS
//*  BOTH DAILY PAYROLL PURGE AND MO COMPUTE PURGE
//*  (PPSPOPRG, PPSPP910)
//*  SO THE FILES WILL REMAIN ON DISK. (10/08/2008-BEV)
//*====================================================                 00110000
//*
/*ROUTE PRINT RMT10
// JCLLIB ORDER=APCP.PROCLIB                                            00053000
//OUT2 OUTPUT CLASS=G,JESDS=ALL,DEST=LOCAL,GROUPID=PRODCTL
//*                                                                     00001600
//*====================================================                 00110000
//* THE FOLLOWING STEPS WILL ALLOCATE PPSP.MEDTRAN.NEW
//* PPSP.MEDTRAN.BW.NEW, PPSP.MEDTRAN.BW.ADJ.NEW
//* SO THAT THE HOSPITAL CAN ELECTRONICALLY TRANSMIT
//* THEIR FILE THE NEXT DAY.
//*
//*  ALWAYS RESTART AT UNCAT1.
//*  IF HAVE PROBLEMS WITH UPLOAD OF MEDTRAN   FILE...CONTACT
//*  GARY NITTOLY
//*====================================================                 00110000
//*  DELETE UNUSED DATASETS                                             00040006
//*====================================================                 00110000
//*                                                                     00090006
//UNCAT1   EXEC PGM=IEFBR14,                                            00170006
//             COND=((7,LT))                                            00000500
//DD1       DD DSN=PPSP.MEDTRAN.NEW,                                    00180013
//             DISP=(MOD,DELETE,DELETE),                                00190006
//             SPACE=(TRK,(0)),                                         00210006
//             UNIT=SYSDA                                               00220009
//*                                                                     00110006
//UNCAT2   EXEC PGM=IEFBR14,                                            00170006
//             COND=((7,LT))                                            00000500
//DD1       DD DSN=PPSP.MEDTRAN.BW.MSG.NEW,                             00180013
//             DISP=(MOD,DELETE,DELETE),                                00190006
//             SPACE=(TRK,(0)),                                         00210006
//             UNIT=SYSDA                                               00220009
//*                                                                     00110006
//UNCAT3   EXEC PGM=IEFBR14,                                            00170006
//             COND=((7,LT))                                            00000500
//DD1       DD DSN=PPSP.MEDTRAN.BW.NEW,                                 00180013
//             DISP=(MOD,DELETE,DELETE),                                00190006
//             SPACE=(TRK,(0)),                                         00210006
//             UNIT=SYSDA                                               00220009
//*                                                                     00110006
//UNCAT4   EXEC PGM=IEFBR14,                                            00170006
//             COND=((7,LT))                                            00000500
//DD1       DD DSN=PPSP.MEDTRAN.BW.ADJ.NEW,                             00180013
//             DISP=(MOD,DELETE,DELETE),                                00190006
//             SPACE=(TRK,(0)),                                         00210006
//             UNIT=SYSDA                                               00220009
//*                                                                     00110006
//UNCAT5   EXEC PGM=IEFBR14,                                            00170006
//             COND=((7,LT))                                            00000500
//DD1       DD DSN=PPSP.MEDTRAN.BW.ADJ.MSG.NEW,                         00180013
//             DISP=(MOD,DELETE,DELETE),                                00190006
//             SPACE=(TRK,(0)),                                         00210006
//             UNIT=SYSDA                                               00220009
//*                                                                     00110006
//*====================================================                 00110000
//* CREATE NEW DATASETS                                                 00150006
//*====================================================                 00110000
//STEP1    EXEC PGM=IEFBR14,                                            00170006
//             COND=((7,LT))                                            00000500
//DD1       DD DSN=PPSP.MEDTRAN.NEW,                                    00180013
//             DISP=(,CATLG,DELETE),                                    00190006
//             DCB=(RECFM=FB,LRECL=200,BLKSIZE=23400),                  00200016
//             SPACE=(TRK,(75,75),RLSE),                                00210006
//             UNIT=SYSDA                                               00220009
//*
//*
//STEP2    EXEC PGM=IEFBR14,                                            00170006
//             COND=((7,LT))                                            00000500
//DD1       DD DSN=PPSP.MEDTRAN.BW.NEW,                                 00180013
//             DISP=(,CATLG,DELETE),                                    00190006
//             DCB=(RECFM=FB,LRECL=200,BLKSIZE=23400),                  00200016
//             SPACE=(TRK,(75,75),RLSE),                                00210006
//             UNIT=SYSDA                                               00220009
//*
//STEP3    EXEC PGM=IEFBR14,                                            00240008
//             COND=((7,LT))                                            00000500
//DD1       DD DSN=PPSP.MEDTRAN.BW.MSG.NEW,                             00250014
//             DISP=(,CATLG,DELETE),                                    00260008
//             DCB=(RECFM=FB,LRECL=80,BLKSIZE=8000),                    00271012
//             SPACE=(TRK,(1,1),RLSE),                                  00280008
//             UNIT=SYSDA                                               00290009
//*
//STEP4    EXEC PGM=IEFBR14,                                            00170006
//             COND=((7,LT))                                            00000500
//DD1       DD DSN=PPSP.MEDTRAN.BW.ADJ.NEW,                             00180013
//             DISP=(,CATLG,DELETE),                                    00190006
//             DCB=(RECFM=FB,LRECL=200,BLKSIZE=23400),                  00200016
//             SPACE=(TRK,(75,75),RLSE),                                00210006
//             UNIT=SYSDA                                               00220009
//*
//STEP5    EXEC PGM=IEFBR14,                                            00240008
//             COND=((7,LT))                                            00000500
//DD1       DD DSN=PPSP.MEDTRAN.BW.ADJ.MSG.NEW,                         00250014
//             DISP=(,CATLG,DELETE),                                    00260008
//             DCB=(RECFM=FB,LRECL=80,BLKSIZE=8000),                    00271012
//             SPACE=(TRK,(1,1),RLSE),                                  00280008
//             UNIT=SYSDA                                               00290009
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEK4 EXEC STOPZEK4                                                00010000
//*                                                                     00170000
//*================ E N D  O F  J C L  PPSPA11N ========                00250000
