//SETBSL06 JOB SYS000,'PC-0903',                                        00000132
//         MSGLEVEL=(1,1),                                              00000200
//         REGION=4096K,                                                00000300
//         NOTIFY=APCDBM,                                               00000411
//         CLASS=D,                                                     00000500
//         MSGCLASS=F                                                   00000600
//*++++++++++++++++++++++++++                                           00000700
//*    ZEKE EVENT # 1228  ** SPECIAL REQUEST **                         00000827
//*++++++++++++++++++++++++++                                           00000900
//*====================================================                 00001000
//STEP1    EXEC PGM=IEBGENER                                            00001200
//SYSPRINT DD SYSOUT=*                                                  00001300
//SYSUT1   DD DSN=PPSP.BSLPGM.CTL.JUN,DISP=SHR                          00001400
//SYSUT2   DD DSN=PPSP.BSLPGM.CTL,DISP=SHR                              00001500
//SYSIN    DD DUMMY                                                     00001600
//*====================================================                 00001700
//STEP2    EXEC PGM=IEBGENER                                            00001900
//SYSPRINT DD SYSOUT=*                                                  00002000
//SYSUT1   DD DSN=PPSP.BSL9000.JUN,DISP=SHR                             00002100
//SYSUT2   DD DSN=PPSP.BSL9000,DISP=SHR                                 00002200
//SYSIN    DD DUMMY                                                     00002300
//*====================================================                 00002400
//*  VALUES ARE 'YYMM'                                                  00006217
//*====================================================                 00006317
//STEP4    EXEC PGM=ZEKESET                                             00008000
//SYSPRINT DD SYSOUT=*                                                  00009000
//SYSIN    DD *                                                         00010000
  SET VAR $PSTCYM EQ 'ST2006'                                           00020036
  SET VAR $PSTYYMM EQ '2006'                                            00021036
/*                                                                      00030000
//*============================================================         00030100
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00030202
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00030300
//*============================================================         00030400
//STOPZEKE EXEC STOPZEKE                                                00030501
//*                                                                     00031800
//*================ E N D  O F  J C L  SETBSL06 ========                00032000
