//PPSPM750 JOB SYS000,'PRDCNTL',MSGLEVEL=(1,1),                         00000100
//         CLASS=F,NOTIFY=APCDBM,REGION=0M,                             00000200
//         MSGCLASS=F                                                   00000300
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 053
//*++++++++++++++++++++++++++
//OUT1 OUTPUT CLASS=X,FORMS=PC08,JESDS=ALL,DEFAULT=YES,GROUPID=PRDCTL
//OUT2 OUTPUT FORMS=STD,GROUPID=TRNSMITL
// JCLLIB ORDER=APCP.PROCLIB                                            00053000
//*                                                                     00000600
//*========================================================
//*  COPY PPI7501 FOR BFS IDOCS
//*========================================================
//PPSPMT50 EXEC PPSPMT50,CCYYMM='$PCRCYM',
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//*                                                                     00001600
//*========================================================
//*       EMAIL NOTIFICATION TO BFS
//*========================================================
//MAILIT EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSPROC DD DSN=SYST.T.CLIST,DISP=SHR
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=*
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:
//DESTIDS DD *
DMESERVE@UCSD.EDU
/*
//* EMAIL TEXT FOLLOWS:
//NOTES DD *
REMINDER:

THE 'GAOP.PPI7501.$PCRCYM' FILE (UCRS CONTRIBUTION LIST)
HAS BEEN CREATED ON THE MAINFRAME AND IS AVAILABLE FOR FTP TO BFS.

 THANK YOU,
 MGR, PRODUCTION CONTROL
 EMAIL: ACT-PRODCONTROL@UCSD.EDU


/*
//* CONTROL REPORT FILENAME FOLLOWS:
//SYSTSIN   DD *
 %MAILRPT -
 'PPSP.DUMMY.MAIL.MESSAGE' -
 SUBJECT('IMAGING FILE FOR BFS')
/*
//*---
//* THE 'DUMMY' MESSAGE MUST BE USED ABOVE...
//*============================================================
//*  FTP TO BFS
//*============================================================
//SUBBFSFT EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2300) REBUILD'
/*
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEKE EXEC STOPZEKE                                                00010000
//*                                                                     00170000
//*================ E N D  O F  J C L  PPSPM750 ========                00250001
