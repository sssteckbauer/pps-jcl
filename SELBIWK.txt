//SELBIWK  JOB (SYS000),'PAUL',MSGLEVEL=(1,1),                          00001000
//        MSGCLASS=X,CLASS=B,NOTIFY=APCDBM,REGION=2M                    00002000
/*ROUTE PRINT RMT1                                                      00002100
//*====================================================                 00002200
//*   THIS JCL EXTRACTS "BIWEEKLY" RA TRANSACTIONS FROM THE RATRANS     00002400
//*   FILE SO THAT THEY CAN BE FED INTO A BI PAYCYCLE (OPTIONAL)        00002500
//*====================================================                 00002600
//STEP1    EXEC PGM=SYNCSORT                                            00002800
//SYSOUT   DD SYSOUT=*                                                  00002900
//SORTIN   DD DSN=PPSP.RETRO.A9105.RATRANS.FINAL,DISP=SHR               00003000
//SORTOUT  DD DSN=PPSP.RETRO.A9105.RATRANS.BIWEEKLY,                    00003100
//         DISP=(,CATLG,DELETE),                                        00003200
//         UNIT=SYSDA,SPACE=(TRK,(05,5),RLSE),                          00004000
//         DCB=(RECFM=FB,LRECL=102,BLKSIZE=22950),                      00005000
//         VOL=SER=PRDATA                                               00006000
//SORTWK01 DD UNIT=SYSDA,SPACE=(TRK,60)                                 00007000
//SYSIN    DD *                                                         00008000
    SORT   FIELDS=COPY                                                  00009000
      INCLUDE COND=(67,2,CH,EQ,C'BA')                                   00010000
           END                                                          00020000
/*                                                                      00030000
//*================ E N D  O F  J C L  SELBIWK  ========                00040000
