//MAKETAPE JOB SYS000,'PAUL',MSGLEVEL=(1,1),                            00010004
//        NOTIFY=APCDBM,CLASS=A,MSGCLASS=F                              00020004
//*                                                                     00030000
//*ROUTE PRINT RMT1                                                     00040004
//*                                                                     00050000
//*************** PRUDENTIAL HEALTH M9404 ***************************** 00060003
//*                (UNCOMPRESSED BY DEFAULT) ************************** 00061002
//*                                                                     00070000
//PH565    EXEC PGM=SYNCSORT,                                           00080000
//             REGION=4096K                                             00090000
//SORTIN    DD DSN=PPSP.PPP565.PH,                                      00100005
//             DISP=OLD                                                 00110000
//SORTOUT   DD DSN=UCSDPAY.PH,                                          00120006
//             DISP=(NEW,KEEP,DELETE),                                  00130000
//             DCB=(RECFM=FB,LRECL=246,BLKSIZE=23370,DEN=3),            00140002
//             LABEL=(1,SL,,,RETPD=14),                                 00150000
//             UNIT=CTAPE                                               00160000
//SYSOUT    DD DSN=&&PH565,                                             00170000
//             DISP=(NEW,PASS),                                         00180000
//             DCB=(RECFM=F,BLKSIZE=132,DSORG=DA),                      00190000
//             SPACE=(80,100),                                          00200000
//             UNIT=SYSDA                                               00210000
//SYSUDUMP  DD SYSOUT=D,                                                00220000
//             DEST=LOCAL                                               00230000
//SYSPRINT  DD SYSOUT=*                                                 00240000
//SYSIN     DD DSN=PPSP.PARMLIB(COPYFILE),                              00250000
//             DISP=SHR                                                 00260000
//*                                                                     00270000
//*======= PRUDENTIAL TAPE TRANSMITTAL =================================00280000
//TRANSMIT EXEC XMITPROC,REFCARD='PH565.SORTOUT'                        00290000
//XMIT.RECCOUNT DD DSN=&&PH565,                                         00300000
//             DISP=(OLD,DELETE)                                        00310000
//XMIT.SYSOUT DD SYSOUT=*                                               00320000
//XMIT.MAILTO DD *                                                      00330000
1. PRODUCTION CONTROL                                                   01840004
2. PRUDENTIAL                                                           01850004
   ATTN: DAPHNE LARANETA-DAVIS                                          01860004
   GISD -- WHW-1                                                        01870004
   5800 CANOGA AVENUE                                                   01880004
   WOODLAND HILLS, CA. 91367                                            01890004
/*                                                                      01900004
//XMIT.COMMENTS DD *                                                    01910004
PRUDENTIAL HEALTH PLAN (RERUN FOR JULY 1994)                            01920007
************************ UCSD CONTACTS:                                 01930004
*  PLEASE RETURN THE   * PAUL SERMAK/DOUG MESERVE                     01940008
*   CARTRIDGE TAPE     * (858)534-2443/534-2447                         01950008
************************                                                01960004
/*                                                                      01970004
