//UNLDPAY  JOB SYS000,'PAUL - S029',MSGLEVEL=(1,1),                     00000100
//        NOTIFY=APCDBM,CLASS=A,REGION=4096K,                           00000206
//        MSGCLASS=X                                                    00000300
//JOBLIB   DD DSN=PPSP.ONLINE.LOADLIB,
//            DISP=SHR
//         DD DSN=PPSP.LOADLIB,
//            DISP=SHR
//         DD DSN=DB2P.DCA.V2R1M0.LOAD,
//            DISP=SHR
//         DD DSN=DB2P.DSNEXIT,
//            DISP=SHR
//         DD DSN=DB2P.DSNLOAD,
//            DISP=SHR
// JCLLIB ORDER=APCP.PROCLIB                                            00053000
/*ROUTE PRINT RMT10                                                     00001500
//*
//********************************************************************  MLG1187
//*   BACKUP  PAYCABY FILE.                                             MLG1187
//********************************************************************  MLG1187
//DELBKUP  EXEC PGM=IDCAMS                                              00040000
//SYSPRINT DD SYSOUT=*                                                  00050000
//SYSIN    DD *                                                         00080000
     DELETE (PPSP.SEQ.PAYCABY.PROD.JD91155) PURGE                       00010001
//*                                                                     PAYEAUD
//BKUPAUD   EXEC PGM=IDCAMS                                             MLG1187
//SYSPRINT  DD   SYSOUT=*                                               MLG1187
//OUTDD     DD DSN=PPSP.SEQ.PAYCABY.PROD.JD91155,                       MLG1187
//          DISP=(,CATLG,DELETE),
//          UNIT=SYSDA,DCB=(RECFM=FB,LRECL=143,BLKSIZE=6292),           MLG1187
//          SPACE=(TRK,(10,10),RLSE),
//          VOL=SER=PRDATA
//SYSIN     DD *                                                        MLG1187
     REPRO -                                                            00010000
       INDATASET (PPSP.PCICSV.PAYCABY) -                                00020000
       OUTFILE (OUTDD) -                                                00030000
       REPLACE ;                                                        00040000
//*                                                                     MLG1187
//********************************************************************  MLG1187
//*   BACKUP  PAYTHF FILE.                                              MLG1187
//********************************************************************  MLG1187
//DELBKUP  EXEC PGM=IDCAMS                                              00040000
//SYSPRINT DD SYSOUT=*                                                  00050000
//SYSIN    DD *                                                         00080000
     DELETE (PPSP.SEQ.PAYTHF.PROD.JD91155) PURGE                        00010001
//*                                                                     PAYEAUD
//BKUPAUD   EXEC PGM=IDCAMS                                             MLG1187
//SYSPRINT  DD   SYSOUT=*                                               MLG1187
//OUTDD     DD DSN=PPSP.SEQ.PAYTHF.PROD.JD91155,                        MLG1187
//          DISP=(,CATLG,DELETE),
//          UNIT=SYSDA,DCB=(RECFM=FB,LRECL=159,BLKSIZE=23373),          MLG1187
//          SPACE=(TRK,(10,10),RLSE),
//          VOL=SER=PRDATA
//SYSIN     DD *                                                        MLG1187
     REPRO -                                                            00010000
       INDATASET (PPSP.PCICSV.PAYTHF) -                                 00020000
       OUTFILE (OUTDD) -                                                00030000
       REPLACE ;                                                        00040000
//*                                                                     MLG1187
//
