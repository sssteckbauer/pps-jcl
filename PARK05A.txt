//PARK05A  JOB (SYS000),'RENA - 0929',CLASS=K,MSGLEVEL=(1,1),
//        MSGCLASS=X,NOTIFY=DEVRWQ,REGION=0M
//*       RESTART=(UNCATLOG)
//*       RESTART=(STEP00)
//JOBLIB    DD DSN=PPSP.LOADLIB,DISP=SHR                                00090000
//          DD DSN=DB2P.DCA.LOAD,DISP=SHR                               00100000
//          DD DSN=DB2P.DSNEXIT,DISP=SHR                                00110000
//          DD DSN=DB2P.DSNLOAD,DISP=SHR                                00120000
//*         DD DSN=SYS2.LE370.SCEERUN,DISP=SHR                          00001600
//*         DD DSN=SYS2.COB2.COB2LIB,DISP=SHR                           00130000
/*ROUTE PRINT RMT1
//*
//**********************************************************************
//*     PPP060 IS NOW RUN AT UCOP TO CREATE RATE CHANGE TRANSACTIONS
//*     PPSP.PPP060.CARDFIL.D053105 CONTAINS THE PPP060 RUN PARAMETERS
//*          PPP060-SPEC060105
//*          20880120001250
//*          20880180001900
//*          20880190002000
//*          20880355003750
//*          20880415004350
//*          20880660007100
//*          20880710007500
//*          20880770008300
//*          20880830008700
//*          20881430015400
//*          20881540016200
//*          20890120001250
//*          20890180001900
//*          20890190002000
//*          20890355003750
//*          20890415004350
//*          20890660007100
//*          20890710007500
//*          20890770008300
//*          20890830008700
//*          20891430015400
//*          20891540016200
//*          24000120001250
//*          24000180001900
//*          24000190002000
//*          24000355003750
//*          24000415004350
//*          24000660007100
//*          24000710007500
//*          24000770008300
//*          24000830008700
//*          24001430015400
//*          24001540016200
//*          24010120001250
//*          24010180001900
//*          24010190002000
//*          24010355003750
//*          24010415004350
//*          24010660007100
//*          24010710007500
//*          24010770008300
//*          24010830008700
//*          24011430015400
//*          24011540016200
//*
//*     IT WAS NOT NECESSARY TO USE 999999 RATES TO KEEP FROZEN
//*     PARKING RATES FROM APPEARING ON THE EXCEPTION REPORT;
//*     HOWEVER, THE JCL TO EXCLUDE THEM WAS STILL LEFT IN THIS JOB.
//*
//**********************************************************************
//*
//*****************************************************************
//* NOTE:  TRANSACTION HAS EFF. DATE OF 060105 (FROM EXPPP060) AND
//*        GTN         HAS EFF. DATE OF 060105  (SEE COLS. 31-42)
//*                    BECAUSE PARKING DEDUCTIONS ARE TAKEN ONE MONTH
//*                    IN ADVANCE AND CHANGES ARE FOR B10506 COMPUTE
//*                    ON 6/09/05
//*
//*        BOTH DATES SHOULD BE 060105 WHEN THEY FEED INTO
//*        EDB MAINTENANCE
//*
//*        EDIT PPST.EDBTRANS.BATCH TO REFLECT BATCH HEADER
//*        INFORMATION APPROPRIATE FOR RUN
//*
//* NOTE:  PHASE I  OF THIS JOB EXLUDES SPECIFIC UNIONS (RX,TX,NX)
//*
//*        PPSP.PARK05A.PPP060.EDBTRANS.FINAL
//*        SHOULD BE FTPED TO UCOP FOR EDB TRANSACTION-DRIVEN
//*        MAINTENANCE
//*
//*****************************************************************
//*    PHASE I
//*****************************************************************
//***************** UNCATALOG ***************************************** 00030000
//*                                                                     00040000
//STEP00   EXEC PGM=IEFBR14                                             00080000
//*            REGION=300K                                              00090000
//DD1       DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.T1,                     00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD2       DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.T2,                     00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD3       DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.STU,                    00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD4       DD DSN=PPSP.PARK05A.PPP060.EXCLUDED,                        00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//*                                                                     00100000
//*****************************************************************
//STEP01   EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PXT.XDSX,
//            DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.T1,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=50,BLKSIZE=23400),
//*           VOL=SER=APC003,
//            SPACE=(TRK,(60,15),RLSE),
//            DISP=(NEW,CATLG,CATLG)
//SYSIN    DD *
     SORT FIELDS=(1,9,CH,A),EQUALS
     SUM  FIELDS=(NONE)
     INCLUDE COND=(((70,2,CH,EQ,C'NX',OR,
                     70,2,CH,EQ,C'RX',OR,
                     70,2,CH,EQ,C'TX'),AND,
                    73,1,CH,EQ,C'C'),AND,
                   (183,6,CH,LE,C'050601',OR,
                    183,6,CH,GT,C'500000'),AND,
                   ((189,6,CH,GE,C'050601',AND,
                     189,6,CH,LT,C'500000'),OR,
                    (189,6,CH,EQ,C'999999')))
     OUTREC FIELDS=(1,9,70,2,X,73,1,70,2,70,2,26,4,106,26,3X)
/*
//*****************************************************************
//STEP02   EXEC PGM=PPL802
//STEPLIB  DD DSN=PPSP.LOADLIB,DISP=SHR
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//CNTLCARD DD *
RECSIZE     50 400
TARGET   9   1   1
RESULT   1  12 148
DEFAULT  ' '
/*
//TABLEFIL DD DSN=PPSP.PXT.XEMX,DISP=SHR
//DATAFILE DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.T1,DISP=SHR
//PRINTER  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//SYSDBOUT DD SYSOUT=*
//SYSABEND DD SYSOUT=*
//ABENDAID DD SYSOUT=*
//OUTPUT   DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.T2,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=500,BLKSIZE=23000),
//*           VOL=SER=APC003,
//            SPACE=(TRK,(300,150),RLSE),
//            DISP=(NEW,CATLG,CATLG)
//REPORT   DD SYSOUT=*
//CNTLRPT  DD SYSOUT=*
//*****************************************************************
//STEP08A  EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.T2,DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.EXCLUDED,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=50,BLKSIZE=23400),
//            SPACE=(TRK,(60,15),RLSE),
//*           VOL=SER=APC003,
//            DISP=(NEW,CATLG,CATLG)
//SYSIN    DD *
     SORT FIELDS=(1,9,CH,A)
     INCLUDE COND=(12,1,CH,LT,C'3')
     OUTREC FIELDS=(1,50)
/*
//*****************************************************************
//STEP08B  EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.T2,DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.STU,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=50,BLKSIZE=23400),
//            SPACE=(TRK,(60,15),RLSE),
//*           VOL=SER=APC003,
//            DISP=(NEW,CATLG,CATLG)
//SYSIN    DD *
     SORT FIELDS=(1,9,CH,A)
     OMIT    COND=(12,1,CH,LT,C'3')
     OUTREC FIELDS=(1,50)
/*
//*
//***************** UNCATALOG ***************************************** 00030000
//*                                                                     00040000
//STEP09   EXEC PGM=IEFBR14                                             00080000
//*            REGION=300K                                              00090000
//DD1       DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.T1,                     00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD2       DD DSN=PPSP.PARK05A.PPP060.EXCLUDED.T2,                     00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//*                                                                     00100000
//*****************************************************************
//*                                                                     00040000
//STEP10   EXEC PGM=IEFBR14                                             00080000
//*            REGION=300K                                              00090000
//DD1       DD DSN=PPSP.PARK05A.PPP060.TRANS.T1,                        00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD2       DD DSN=PPSP.PARK05A.PPP060.TRANS.T2,                        00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD3       DD DSN=PPSP.PARK05A.PPP060.TRANS.T3,                        00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD4       DD DSN=PPSP.PARK05A.PPP060.TRANS.T4,                        00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD5       DD DSN=PPSP.PARK05A.PPP060.TRANS.ALL,                       00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD6       DD DSN=PPSP.PARK05A.PPP060.TRANS.EXCLUDED,                  00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD7       DD DSN=PPSP.PARK05A.PPP060.TRANS.INCLUDED,                  00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD8       DD DSN=PPSP.PARK05A.PPP060.TRANS.OMITTED,                   00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD9       DD DSN=PPSP.PARK05A.PPP060.TRANS.FINAL,                     00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//*                                                                     00100000
//*****************************************************************
//STEP11A  EXEC PGM=PPL802
//STEPLIB  DD DSN=PPSP.LOADLIB,DISP=SHR
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//CNTLCARD DD *
RECSIZE    132  50
TARGET   9   4   1
RESULT  30 103  10
DEFAULT  ' '
/*
//TABLEFIL DD DSN=PPSP.PARK05A.PPP060.EXCLUDED,DISP=SHR
//DATAFILE DD DSN=PPSP.PARK05A.PPP060.EDBTRANS.ORIG,DISP=SHR
//PRINTER  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//SYSDBOUT DD SYSOUT=*
//SYSABEND DD SYSOUT=*
//ABENDAID DD SYSOUT=*
//OUTPUT   DD DSN=PPSP.PARK05A.PPP060.TRANS.T1,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=500,BLKSIZE=23000),
//            SPACE=(TRK,(300,150),RLSE),
//*           VOL=SER=APC003,
//            DISP=(NEW,CATLG,CATLG)
//REPORT   DD SYSOUT=*
//CNTLRPT  DD SYSOUT=*
//*****************************************************************
//STEP11B  EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.TRANS.T1,
//            DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.TRANS.ALL,
//            DISP=(NEW,CATLG,CATLG),
//            DCB=(RECFM=FB,LRECL=132,BLKSIZE=23364),
//*           VOL=SER=APC003,
//            SPACE=(TRK,(60,15),RLSE),
//            UNIT=SYSDA
//SYSIN    DD *
              SORT FIELDS=(4,9,CH,A)
              OUTREC FIELDS=(1,132)
/*
//*****************************************************************
//STEP11C  EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.TRANS.ALL,
//            DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.TRANS.OMITTED,
//            DISP=(NEW,CATLG,CATLG),
//            DCB=(RECFM=FB,LRECL=132,BLKSIZE=23364),
//*           VOL=SER=APC003,
//            SPACE=(TRK,(60,15),RLSE),
//            UNIT=SYSDA
//SYSIN    DD *
              SORT FIELDS=(4,9,CH,A)
              INCLUDE COND=(24,7,CH,EQ,C'0099999')
              OUTREC FIELDS=(1,132)
/*
//*****************************************************************
//STEP11D  EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.TRANS.ALL,
//            DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.TRANS.EXCLUDED,
//            DISP=(NEW,CATLG,CATLG),
//            DCB=(RECFM=FB,LRECL=132,BLKSIZE=23364),
//*           VOL=SER=APC003,
//            SPACE=(TRK,(60,15),RLSE),
//            UNIT=SYSDA
//SYSIN    DD *
              SORT FIELDS=(4,9,CH,A)
              OMIT    COND=(24,7,CH,EQ,C'0099999',OR,
                            129,2,CH,EQ,C'  ')
              OUTREC FIELDS=(1,132)
/*
//*****************************************************************
//STEP11E  EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.TRANS.ALL,
//            DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.TRANS.INCLUDED,
//            DISP=(NEW,CATLG,CATLG),
//            DCB=(RECFM=FB,LRECL=132,BLKSIZE=23364),
//*           VOL=SER=APC003,
//            SPACE=(TRK,(60,15),RLSE),
//            UNIT=SYSDA
//SYSIN    DD *
              SORT FIELDS=(4,9,CH,A)
              OMIT    COND=(24,7,CH,EQ,C'0099999',OR,
                            129,2,CH,NE,C'  ')
              OUTREC FIELDS=(1,132)
/*
//*****************************************************************
//STEP12F  EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.TRANS.INCLUDED,
//            DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.TRANS.FINAL,
//            DISP=(NEW,CATLG,CATLG),
//            DCB=(RECFM=FB,LRECL=102,BLKSIZE=23358),
//*           VOL=SER=APC003,
//            SPACE=(TRK,(60,15),RLSE),
//            UNIT=SYSDA
//SYSIN    DD *
              SORT FIELDS=(4,9,CH,A)
              OUTREC FIELDS=(1,102)
/*
//*****************************************************************
//*                                                                     00040000
//STEP19   EXEC PGM=IEFBR14                                             00080000
//*            REGION=300K                                              00090000
//DD1       DD DSN=PPSP.PARK05A.PPP060.TRANS.T1,                        00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD2       DD DSN=PPSP.PARK05A.PPP060.TRANS.T2,                        00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD3       DD DSN=PPSP.PARK05A.PPP060.TRANS.T3,                        00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD4       DD DSN=PPSP.PARK05A.PPP060.TRANS.T4,                        00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//*                                                                     00040000
//*****************************************************************
//*                                                                     00040000
//STEP20   EXEC PGM=IEFBR14                                             00080000
//*            REGION=300K                                              00090000
//DD1       DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T0,                 00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD2       DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T1,                 00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD3       DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T2,                 00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD4       DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T3,                 00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD5       DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T4,                 00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD6       DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T5,                 00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//DD7       DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT,                    00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//*                                                                     00040000
//*****************************************************************
//STEP21   EXEC PGM=PPL802
//STEPLIB  DD DSN=PPSP.LOADLIB,DISP=SHR
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//CNTLCARD DD *
RECSIZE    140 400
TARGET   9   4   1
RESULT   1 105  49
DEFAULT  OFF
/*
//TABLEFIL DD DSN=PPSP.PXT.XEMX,
//            DISP=SHR
//DATAFILE DD DSN=PPSP.PARK05A.PPP060.TRANS.ALL,
//            DISP=SHR
//PRINTER  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//SYSDBOUT DD SYSOUT=*
//SYSABEND DD SYSOUT=*
//ABENDAID DD SYSOUT=*
//OUTPUT   DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T0,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=500,BLKSIZE=23000),
//            SPACE=(TRK,(300,150),RLSE),
//*           VOL=SER=APC003,
//            DISP=(NEW,CATLG,CATLG)
//REPORT   DD SYSOUT=*
//CNTLRPT  DD SYSOUT=*
//*****************************************************************
//STEP22   EXEC PGM=PPL802
//STEPLIB  DD DSN=PPSP.LOADLIB,DISP=SHR
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//CNTLCARD DD *
RECSIZE    140  80
TARGET   7  24   1
RESULT   8 133   9
DEFAULT  ' '
/*
//TABLEFIL DD *
0099999=NOCHANGE
/*
//DATAFILE DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T0,
//            DISP=SHR
//PRINTER  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//SYSDBOUT DD SYSOUT=*
//SYSABEND DD SYSOUT=*
//ABENDAID DD SYSOUT=*
//OUTPUT   DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T1,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=500,BLKSIZE=23000),
//            SPACE=(TRK,(300,150),RLSE),
//*           VOL=SER=APC003,
//            DISP=(NEW,CATLG,CATLG)
//REPORT   DD SYSOUT=*
//CNTLRPT  DD SYSOUT=*
//*****************************************************************
//STEP23   EXEC PGM=PPL802
//STEPLIB  DD DSN=PPSP.LOADLIB,DISP=SHR
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//CNTLCARD DD *
RECSIZE    140  80
TARGET   2 103   1
TARGET   8 133   4
RESULT   8 133  13
DEFAULT  OFF
/*
//TABLEFIL DD *
NX/        =NX-EXCL
RX/        =RX-EXCL
TX/        =TX-EXCL
  /        =CHANGE
/*
//DATAFILE DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T1,
//            DISP=SHR
//PRINTER  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//SYSDBOUT DD SYSOUT=*
//SYSABEND DD SYSOUT=*
//ABENDAID DD SYSOUT=*
//OUTPUT   DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T2,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=500,BLKSIZE=23000),
//            SPACE=(TRK,(300,150),RLSE),
//*           VOL=SER=APC003,
//            DISP=(NEW,CATLG,CATLG)
//REPORT   DD SYSOUT=*
//CNTLRPT  DD SYSOUT=*
//*****************************************************************
//STEP24   EXEC PGM=PPL802
//STEPLIB  DD DSN=PPSP.LOADLIB,DISP=SHR
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//CNTLCARD DD *
RECSIZE    140  80
TARGET   1 105   1
TARGET   8 133   3
RESULT   8 133  12
DEFAULT  OFF
/*
//TABLEFIL DD *
S/NX-EXCL =SEP-EXC
S/RX-EXCL =SEP-EXC
S/TX-EXCL =SEP-EXC
S/CHANGE  =SEP-CHG
/*
//DATAFILE DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T2,
//            DISP=SHR
//PRINTER  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//SYSDBOUT DD SYSOUT=*
//SYSABEND DD SYSOUT=*
//ABENDAID DD SYSOUT=*
//OUTPUT   DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T3,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=500,BLKSIZE=23000),
//            SPACE=(TRK,(300,150),RLSE),
//*           VOL=SER=APC003,
//            DISP=(NEW,CATLG,CATLG)
//REPORT   DD SYSOUT=*
//CNTLRPT  DD SYSOUT=*
//*****************************************************************
//STEP25   EXEC PGM=PPL802
//STEPLIB  DD DSN=PPSP.LOADLIB,DISP=SHR
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//CNTLCARD DD *
RECSIZE    140  80
TARGET   9   4   1
RESULT   8 133  11
DEFAULT  OFF
/*
//TABLEFIL DD *
000999999=SPC-EXCL
/*
//DATAFILE DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T3,
//            DISP=SHR
//PRINTER  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//SYSDBOUT DD SYSOUT=*
//SYSABEND DD SYSOUT=*
//ABENDAID DD SYSOUT=*
//OUTPUT   DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T4,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=500,BLKSIZE=23000),
//            SPACE=(TRK,(300,150),RLSE),
//*           VOL=SER=APC003,
//            DISP=(NEW,CATLG,CATLG)
//REPORT   DD SYSOUT=*
//CNTLRPT  DD SYSOUT=*
//*****************************************************************
//STEP28   EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT.T4,
//            DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT,
//            DISP=(NEW,CATLG,CATLG),
//            DCB=(RECFM=FB,LRECL=140,BLKSIZE=23380),
//            SPACE=(TRK,(60,15),RLSE),
//*           VOL=SER=APC003,
//            UNIT=SYSDA
//SYSIN    DD *
              SORT FIELDS=(133,8,CH,A,4,9,CH,A)
              OUTREC FIELDS=(1,140)
/*
//*****************************************************************
//STEP30   EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT,DISP=SHR
//SORTOUT  DD SYSOUT=*
//SYSIN    DD *
  SORT FIELDS=(133,8,CH,A,4,9,CH,A)
  OMIT COND=((133,8,CH,EQ,C'EXCL-99 ',OR,
              133,8,CH,EQ,C'CHANGE  '))
  OUTFIL HEADER2=(1:'DATE:  ',&DATE,
    21:'   PARK05A RATE CHANGE AUDIT REPORT   ',
                 69:'PAGE',&PAGE,//,
    21:'     *** TRANSACTION EXCLUDED ***     ',//,
    21:'        - -  BY CATEGORY  - -         ',//,
    21:'      ( EFFECTIVE:  06/01/2005 )      ',//,
       '      TRANSACTION IMAGE          ',
       'EMPL #  GTN  AMOUNT  ',
       'NAME                TITL',/,
       '------------------------------   ',
       '------  ---  ------  ',
       '------------------  ----',/),
         OUTREC=(1,30,3X,7,6,2X,20,3,2X,
                 24,7,ZD,EDIT=(IIT.TT),2X,
                 115,18,2X,111,4,2X,
                 10X,/),
       SECTIONS=(133,8,SKIP=P,
          HEADER3=('CATEGORY:  ',133,8,2X,/),PAGEHEAD)
/*
//*****************************************************************
//*                                                                     00040000
//STEP40   EXEC PGM=IEFBR14                                             00080000
//*            REGION=300K                                              00090000
//DD1       DD DSN=PPSP.PARK05A.PPP060.TRANS.FINAL.INCLUDED,            00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD2       DD DSN=PPSP.PARK05A.PPP060.TRANS.FINAL.EXCLUDED,            00131000
//             DISP=(MOD,DELETE,DELETE),                                00132000
//             SPACE=(TRK,(0)),                                         00133000
//             UNIT=SYSDA                                               00134000
//*                                                                     00040000
//*****************************************************************
//STEP41   EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT,
//            DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.TRANS.FINAL.INCLUDED,
//            DISP=(NEW,CATLG,CATLG),
//            DCB=(RECFM=FB,LRECL=102,BLKSIZE=23358),
//            SPACE=(TRK,(60,15),RLSE),
//*           VOL=SER=APC003,
//            UNIT=SYSDA
//SYSIN    DD *
     SORT FIELDS=(1,9,CH,A)
     OMIT    COND=((133,8,CH,EQ,C'NX-EXCL ',OR,
                    133,8,CH,EQ,C'RX-EXCL ',OR,
                    133,8,CH,EQ,C'TX-EXCL ',OR,
                    133,8,CH,EQ,C'SPC-EXCL',OR,
                    133,8,CH,EQ,C'********'),OR,
                   (24,7,CH,EQ,C'0099999'),OR,
                   (105,1,CH,EQ,C'S'))
     OUTREC FIELDS=(1,22,C'G',24,7,
                    C'7',20,3,C'E',C'060105',42,61)
/*
//*****************************************************************
//STEP42   EXEC PGM=SYNCSORT
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.PARK05A.PPP060.TRANS.REPORT,
//            DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.TRANS.FINAL.EXCLUDED,
//            DISP=(NEW,CATLG,CATLG),
//            DCB=(RECFM=FB,LRECL=102,BLKSIZE=23358),
//            SPACE=(TRK,(60,15),RLSE),
//*           VOL=SER=APC003,
//            UNIT=SYSDA
//SYSIN    DD *
     SORT FIELDS=(1,9,CH,A)
     INCLUDE COND=((133,8,CH,EQ,C'NX-EXCL ',OR,
                    133,8,CH,EQ,C'RX-EXCL ',OR,
                    133,8,CH,EQ,C'TX-EXCL ',OR,
                    133,8,CH,EQ,C'SPC-EXCL',OR,
                    133,8,CH,EQ,C'********'),OR,
                   (24,7,CH,EQ,C'0099999'),OR,
                   (105,1,CH,EQ,C'S'))
     OUTREC FIELDS=(1,22,C'G',24,7,
                    C'7',20,3,C'E',C'060105',42,61)
/*
//*
//UNCAT43 EXEC PGM=IEFBR14
//DD1     DD DSN=PPSP.PARK05A.PPP060.EDBTRANS.FINAL,
//        DISP=(MOD,DELETE,DELETE),
//        SPACE=(TRK,(0)),UNIT=SYSDA
//*
//STEP43   EXEC PGM=SYNCSORT
//SORTIN   DD DSN=PPST.EDBTRANS.BATCH,DISP=SHR
//         DD DSN=PPSP.PARK05A.PPP060.TRANS.FINAL.INCLUDED,DISP=SHR
//SORTOUT  DD DSN=PPSP.PARK05A.PPP060.EDBTRANS.FINAL,
//         DISP=(,CATLG,DELETE),
//         VOL=SER=APC003,
//*        DCB=(RECFM=FB,LRECL=40,BLKSIZE=23400),
//         UNIT=SYSDA,SPACE=(TRK,(75,75),RLSE)
//SYSOUT   DD SYSOUT=*
//SYSIN    DD *
   SORT FIELDS=COPY
           END
//*
