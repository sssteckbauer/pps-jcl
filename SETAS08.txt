//SETAS08  JOB SYS000,'PAUL -S003',                                     00000100
//         MSGLEVEL=(1,1),                                              00000200
//         REGION=4096K,                                                00000300
//         NOTIFY=APCDBM,                                               00000402
//         CLASS=D,                                                     00000500
//         MSGCLASS=F                                                   00000600
//*++++++++++++++++++++++++++                                           00000700
//*    ZEKE EVENT # 1143                                                00000800
//*++++++++++++++++++++++++++                                           00000900
//STEP1    EXEC PGM=IEBGENER                                            00001000
//SYSPRINT DD SYSOUT=*                                                  00001100
//SYSUT1   DD DSN=PPSP.PARMLIB(AS08),DISP=SHR                           00001200
//SYSUT2   DD DSN=PPSP.PARMLIB(AS30S2),DISP=SHR                         00001300
//SYSIN    DD DUMMY                                                     00002000
//*====================================================                 00002100
//STEP2    EXEC PGM=IEBGENER                                            00004000
//SYSPRINT DD SYSOUT=*                                                  00005000
//SYSUT1   DD DSN=PPSP.PARMLIB(A70308),DISP=SHR                         00006000
//SYSUT2   DD DSN=PPSP.PARMLIB(PPL703C1),DISP=SHR                       00007000
//SYSIN    DD DUMMY                                                     00008000
//*============================================================         00008100
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00008201
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00008300
//*============================================================         00008400
//STOPZEKE EXEC STOPZEKE                                                00008500
//*                                                                     00009800
//*================ E N D  O F  J C L  SETAS08  ========                00010000
