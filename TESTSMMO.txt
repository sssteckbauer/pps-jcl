//TESTSMMO JOB (SYS000),'BEV -PRDCTL',MSGLEVEL=(1,1),
//        MSGCLASS=F,NOTIFY=APCDBM,CLASS=A,REGION=4096K
//*
//*==========================================================
//*    SCHOOL OF MEDICINE - MONTHLY CYCLE
//*==========================================================
//*    SIMPLE SUBMIT
//*    *N.T.E.*
//*==========================================================
//* THIS JOB COMPARES THE **MONTHLY** PAYCYCLE INPUT FILES
//* SUPPLIED BY VARIOUS DEPARTMENTS AGAINST A 'NULL'
//* FILE.  IF THE DEPARTMENT FILES ARE EQUAL TO THE
//* NULL FILE, THEN ** STOP ** PAYROLL PROCESSING AT
//* THIS POINT.... SOMETHING IS WRONG !! ( ONE OR MORE
//* OF THE INPUT FILES IS "EMPTY", AND IT SHOULDN'T BE ).
//*==========================================================
//TEST1    EXEC PGM=FILEMGR,REGION=6M
//SYSPRINT DD  SYSOUT=*
//DD01      DD DISP=SHR,
//             DSN=PPSP.PARMLIB(NULLFILE)
//DD01C     DD DISP=SHR,
//             DSN=PPSP.PTR.SOM.MO.PAYTRANS
//SYSIN    DD  *
$$FILEM VER
$$FILEM DSM TYPE=RECORD,
$$FILEM     SYNCH=READAHEAD,
$$FILEM     LIMIT=100,
$$FILEM     LENGTH=1,
$$FILEM     PACK=UNPACK,
$$FILEM     LIST=DELTA,
$$FILEM     WIDE=YES,
$$FILEM     HILIGHT=YES,
$$FILEM     DDOLD=DD01,
$$FILEM     DDNEW=DD01C
$$FILEM     EOJ
/*
//*
//************ END OF JCL  TESTSMMO ****************************
