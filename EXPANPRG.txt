//PPSPSPEC JOB SYS000,'PRDCTL',MSGLEVEL=(1,1),
//        NOTIFY=APCDBM,CLASS=F,MSGCLASS=X,
//*===============================================
//*       RESTART=(STEP1),
//*===============================================
//        REGION=20M
//*       COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 3057
//*    INACTIVE AS OF 5/12/2011
//*    ALSO REFERENCED IN MEMBER:BACKUPV
//*++++++++++++++++++++++++++
//*******************************************************
//*    DB2 V10.0 UPGRADE - FP6207 (DEVJFV) 11/01/2014   *
//*    REMOVED BIND STEP                                *
//*******************************************************
//JOBLIB   DD DSN=PPSP.LOADLIB,
//            DISP=SHR
//         DD DSN=DB2P.DCA.LOAD,
//            DISP=SHR
//         DD DSN=DB2P.DSNEXIT,
//            DISP=SHR
//         DD DSN=DB2P.DSNLOAD,
//            DISP=SHR
//*====================================================
/*ROUTE PRINT RMT1
//**********************************************************************
// JCLLIB ORDER=APCP.PROCLIB
//**********************************************************************
//*====================================================
//*       THIS JOB WILL DO THE FOLLOWING:
//*
//*          1) CREATE A 'PREPURGE' SNAPSHOT OF THE C1 PAN DATABASE
//*             (PPSP.PAN.*.PREPURGE.JD(JULIAN DATE))
//*             (WILL TAKE ABOUT 1/2 HOUR TO UNLOAD)
//*
//*          2) EXECUTE PROGRAM UC0PNP
//*             (WILL TAKE ABOUT 4 HOURS TO RUN)
//*             THIS PROGRAM WILL PURGE DATA FROM THE UC0PNX,
//*             UC0PNA, UC0PNT, AND UC0PNE TABLES, RETAINING
//*             ONLY AS OF THE CONTROL CARD SPECIFICATION
//*             (I.E. IF "M", THAT MANY MONTHS OF DATA RETAINED
//*                   IF "D", DATA RETAINED AS OF THAT DATE
//*
//*          3) ARCHIVE VIA DUPLICATE TAPE BACKUP THE
//*             PREPURGE PAN UNLOAD AND UC0PNP OUTPUT DATASETS
//*             DSN=PPSP.PANPURGE.JD(JULIAN DATE).*
//*                 WITH RETENTION PERIOD OF 5 YEARS
//*             (WILL TAKE ABOUT 1 HOUR TO RUN)
//*
//*          4) EXECUTE RUNSTATS OF THE PAN TABLES
//*
//*    ** THE NEXT DAY, RUN PPSP.JCL.CNTL(DELPANPG)
//*
//* 4/12/04 CTAPE CHANGED TO CTLIB FOR JD02102 RUN
//*====================================================
//*   CREATE "PREPURGE" SNAPSHOT DATASETS OF PAN DB2 DATABASE
//*   ALWAYS RESTART AT ULOADPAN
//*====================================================
//*
//ULOADPAN EXEC ULOADPAN,PDSTYPE=PREPURGE,CCYYMM=JD$PJDT,VOLSER=SYSDA1,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//UNLOAD.SYSTSIN DD *
 DSN SYSTEM(DB2P)
 RUN PROGRAM(DSUTIAUL) PLAN(DSUTIB00) -
 LIB('DB2P.RUNLIB.LOAD')
//SYSIN     DD *
  PPPDB$PEDBC1..UC0DIR
  PPPDB$PEDBC1..UC0PNX
  PPPDB$PEDBC1..UC0PNA
  PPPDB$PEDBC1..UC0PNE
  PPPDB$PEDBC1..UC0PNT
/*
//*
//*                                                                     00042400
//UC0PNP1  EXEC PGM=IEFBR14                                             00080000
//*            REGION=300K                                              00090000
//DD1       DD DSN=PPSP.PANPURGE.UC0PNPAL.JD$PJDT,                      00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//*                                                                     00140000
//UC0PNP2 EXEC PGM=UC0PNP                                               00050000
//*STEPLIB  DD DSN=PPSP.LOADLIB,DISP=SHR                                00050101
//SYSTSIN  DD  *                                                        00051001
 SYSTEM(DB2P)                                                           00052001
 ERROR(RETURN)                                                          00053001
 PLAN(C1PPOT53)                                                         00054001
/*                                                                      00055001
//SYSDBOUT DD  SYSOUT=*                                                 00060000
//SYSABOUT DD  SYSOUT=*                                                 00070000
//SYSOUT   DD  SYSOUT=*                                                 00080000
//SYSPRINT DD  SYSOUT=*                                                 00090000
//SYSUDUMP DD  SYSOUT=*                                                 00100000
//UC0PNP1  DD  SYSOUT=*                                                 00110000
//UC0PNP2  DD  SYSOUT=*                                                 00120000
//UC0PNPAL DD  DSN=PPSP.PANPURGE.UC0PNPAL.JD$PJDT,                      00130001
//         DISP=(,CATLG,DELETE),                                        00131000
//*        UNIT=SYSDA,SPACE=(CYL,(60,95),RLSE),                         00131000
//         UNIT=SYSALLDA,SPACE=(CYL,(400,95),RLSE),                     00131000
//         DCB=(RECFM=VB,LRECL=906),VOL=SER=APC003                      00132000
//CARDFIL  DD  *                                                        00140000
UC0PNP-SPECD090101                                                      00150000
//*UC0PNP-SPECD090101  USED FOR RUN ON 5/24/02                          00150000
//*UC0PNP-SPECD060101  USED FOR RUN ON 5/17/02                          00150000
//*UC0PNP-SPECD030101  USED FOR RUN ON 4/19/02                          00150000
//*UC0PNP-SPECD120100  USED FOR RUN ON 4/12/02                          00150000
//*UC0PNP-SPECD090100  USED FOR RUN ON 3/30/01                          00150000
//*UC0PNP-SPECD040100  USED FOR RUN ON 3/24/01                          00150000
//*UC0PNP-SPECD010100  USED FOR RUN ON 3/17/01                          00150000
//*UC0PNP-SPECD100199  USED FOR RUN ON 4/29/00                          00150000
//*UC0PNP-SPECD070199  USED FOR RUN ON 4/15/00                          00150000
//*UC0PNP-SPECD040199  USED FOR RUN ON 4/01/00                          00150000
//*UC0PNP-SPECD010199  USED FOR RUN ON 3/25/00                          00150000
//*UC0PNP-SPECD100198                                                   00150000
//*UC0PNP-SPECD100197                                                   00150000
//*UC0PNP-SPECD090196                                                   00150000
//*UC0PNP-SPECD090196                                                   00150000
//*UC0PNP-SPECM03                                                       00150000
//*
//*                                                                     00050001
//********************************************************************* 00060001
//*                                                                     00070001
//*  THIS JOB WILL ARCHIVE FILES RELATED TO PANPURGE PROCESSING         00080001
//*  IN DUPLICATE WITH A FIVE YEAR RETENTION                            00081001
//*                                                                     00090001
//************* FIRST BACKUP **********************                     00100001
//*                                                                     00110001
//STEP1    EXEC PGM=ADRDSSU,                                            00120001
//             REGION=900K                                              00130001
//SYSPRINT  DD SYSOUT=*                                                 00140001
//DD2       DD DSN=PPSP.PANPURGE.JD$PJDT..PRDCTL,                       00150006
//             DISP=(,KEEP,DELETE),                                     00160001
//             DCB=TRTCH=COMP,                                          00161001
//             LABEL=(1,SL,,,RETPD=1825),                               00170001
//*            UNIT=CTAPE,                                              00180001
//             UNIT=CTLIB,                                              00180001
//             VOL=(,RETAIN)                                            00181001
//SYSIN     DD *                                                        00190001
     DUMP -                                                             00200001
       OUTDDNAME (DD2) -                                                00210001
       TOL(ENQF) -                                                      00220001
       SHARE -                                                          00221002
       DATASET (INCLUDE(PPSP.SOURCE.COBOL -                             00230001
                        PPSP.COPYLIB -                                  00232001
                        DBP1.PPPPC1.DBRMLIB -                           00232101
                        DBP1.PPPPC1.DDLLIB -                            00232201
                        DBP1.PPPPC1.DCLLIB -                            00232301
                        PPSP.LOADLIB))                                  00233001
//*                                                                     00234001
//***************** BACKUP OF ALL PANPURGE DATA FILES  **************** 00234201
//*                                                                     00234301
//STEP2    EXEC PGM=ADRDSSU                                             00234401
//*            REGION=900K                                              00234501
//SYSPRINT  DD SYSOUT=*                                                 00234601
//DD2       DD DSN=PPSP.PANPURGE.JD$PJDT..PRDATA,                       00234706
//             DISP=(,KEEP,DELETE),                                     00234801
//             LABEL=(2,SL,,,RETPD=1825),                               00234901
//             DCB=TRTCH=COMP,                                          00235001
//*            UNIT=CTAPE,                                              00235107
//             UNIT=CTLIB,                                              00235107
//             VOL=(,RETAIN,,9,REF=*.STEP1.DD2)                         00235201
//SYSIN     DD *                                                        00235301
     DUMP -                                                             00235401
       OUTDDNAME (DD2) -                                                00235501
       TOL(ENQF) -                                                      00235601
       SHARE -                                                          00235702
       DATASET (INCLUDE(PPSP.PAN.*.PREPURGE.JD$PJDT -                   00235806
                        PPSP.PANPURGE.**))                              00235903
//*                                                                     00236201
//************* SECOND BACKUP **************************                00236301
//*                                                                     00236401
//STEP3    EXEC PGM=ADRDSSU,                                            00237001
//             REGION=900K                                              00238001
//SYSPRINT  DD SYSOUT=*                                                 00239001
//DD2       DD DSN=PPSP.PANPURGE.JD$PJDT..PRDCTL,                       00240006
//             DISP=(,KEEP,DELETE),                                     00250001
//             DCB=TRTCH=COMP,                                          00251001
//             LABEL=(1,SL,,,RETPD=1825),                               00260001
//*            UNIT=CTAPE,                                              00270001
//             UNIT=CTLIB,                                              00270001
//             VOL=(,RETAIN)                                            00271001
//SYSIN     DD *                                                        00280001
     DUMP -                                                             00290001
       OUTDDNAME (DD2) -                                                00300001
       TOL(ENQF) -                                                      00310001
       SHARE -                                                          00311002
       DATASET (INCLUDE(PPSP.SOURCE.COBOL -                             00320001
                        PPSP.COPYLIB -                                  00331001
                        DBP1.PPPPC1.DBRMLIB -                           00331101
                        DBP1.PPPPC1.DDLLIB -                            00331201
                        DBP1.PPPPC1.DCLLIB -                            00331301
                        PPSP.LOADLIB))                                  00332001
//*                                                                     00332101
//***************** BACKUP OF ALL PANPURGE DATA FILES  **************** 00332201
//*                                                                     00332301
//STEP4    EXEC PGM=ADRDSSU                                             00332401
//*            REGION=900K                                              00332501
//SYSPRINT  DD SYSOUT=*                                                 00332601
//DD2       DD DSN=PPSP.PANPURGE.JD$PJDT..PRDATA,                       00332706
//             DISP=(,KEEP,DELETE),                                     00332801
//             LABEL=(2,SL,,,RETPD=1825),                               00332901
//             DCB=TRTCH=COMP,                                          00333001
//*            UNIT=CTAPE,                                              00333107
//             UNIT=CTLIB,                                              00333107
//             VOL=(,RETAIN,,9,REF=*.STEP3.DD2)                         00333201
//SYSIN     DD *                                                        00333301
     DUMP -                                                             00333401
       OUTDDNAME (DD2) -                                                00333501
       TOL(ENQF) -                                                      00333601
       SHARE -                                                          00333702
       DATASET (INCLUDE(PPSP.PAN.*.PREPURGE.JD$PJDT -                   00333806
                        PPSP.PANPURGE.**))                              00333903
//*                                                                     00334001
//*                                                                     00334101
//STEP5    EXEC PGM=XMITTAL                                             00334201
//TAPECARD  DD DSN=*.STEP4.DD2,                                         00335001
//             DISP=OLD,                                                00336001
//             DCB=*.STEP4.DD2,                                         00337001
//             VOL=REF=*.STEP4.DD2                                      00338001
//RECCOUNT  DD DUMMY                                                    00339001
//SYSOUT    DD DUMMY                                                    00340001
//TRANSMIT  DD SYSOUT=C,                                                00350001
//             DEST=LOCAL                                               00360001
//MAILTO    DD *                                                        00370001
OFFSITE STORAGE FACILITY                                                00380001
MIRAMAR ROAD                                                            00390001
/*                                                                      00400001
//COMMENTS  DD *                                                        00410001
BACKUP SET OF PAYROLL TAPES                                             00420001
FOR OFFSITE ARCHIVE STORAGE                                             00430001
/*                                                                      00440001
//*
//*====================================================                 00110000
//*    RUNSTATS                                                         00004800
//*====================================================                 00110000
//*                                                                     00005000
//UTIL     EXEC DSNUPROC,SYSTEM=DB2P,UID='PPPRUNSTATS',UTPROC='',
//             COND=((7,LT))
//DSNUPROC.SYSIN DD *                                                   00005200
RUNSTATS TABLESPACE UC0PAN$PEDBC1..UC0DIR INDEX(ALL) SHRLEVEL REFERENCE
RUNSTATS TABLESPACE UC0PAN$PEDBC1..UC0PNX INDEX(ALL) SHRLEVEL REFERENCE
RUNSTATS TABLESPACE UC0PAN$PEDBC1..UC0PNA INDEX(ALL) SHRLEVEL REFERENCE
RUNSTATS TABLESPACE UC0PAN$PEDBC1..UC0PNE INDEX(ALL) SHRLEVEL REFERENCE
RUNSTATS TABLESPACE UC0PAN$PEDBC1..UC0PNT INDEX(ALL) SHRLEVEL REFERENCE
//*                                                                     00005300
//**************************************************
//WTORMSG  EXEC PGM=IPOWTO                                              00110000
//SYSPRINT  DD DUMMY                                                    00111000
//SYSIN     DD *                                                        00112000
****************************************************
****************************************************
****************************************************

 CICPPSP CAN BE BROUGHT UP WHEN THIS JOB FINISHES !!
 THIS JOB SHOULD HAVE NO CONDITION CODES > 7       !!

 REPLY 'C' TO THIS MESSAGE.
****************************************************
****************************************************
****************************************************
/*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEK4 EXEC STOPZEK4
//*
//*================ E N D  O F  J C L  EXPANPRG ========
