//PPSPM620 JOB SYS000,'PRDCTL',MSGLEVEL=(1,1),                          00000100
//         CLASS=F,NOTIFY=APCDBM,REGION=0M,                             00000200
//*====================================================                 00000300
//*        RESTART=COPYL2,                                              00000400
//*        RESTART=COPYL3,                                              00000400
//*        RESTART=(COPYL4),                                            00000400
//*        RESTART=COPYXMED,                                            00000400
//*        RESTART=COPYL8,                                              00000400
//*====================================================                 00000500
//         MSGCLASS=F                                                   00000600
//*++++++++++++++++++++++++++                                           00000700
//*    ZEKE EVENT # 051                                                 00000800
//*    jcl error check for duplicate datasets                           00000800
//*    restart job from the top                                         00000800
//*++++++++++++++++++++++++++                                           00000900
//*====================================================                 00001000
//*     COPY FILES TO OTHER SYSTEMS                                     00001101
//*====================================================                 00001200
//*                                                                     00001300
// JCLLIB ORDER=APCP.PROCLIB                                            00001400
/*JOBPARM  LINES=1500                                                   00001500
//*                                                                     00002100
//********** UNCATL1 *************                                      00543100
//*                                                                     00543300
//UNCATL1  EXEC PGM=IEFBR14                                             00543400
//DD1       DD DSN=FISP.PPP490.GTN081.$PCRCYM,                          00543500
//             DISP=(MOD,DELETE,DELETE),                                00543600
//             SPACE=(TRK,(0)),                                         00543700
//             UNIT=SYSDA                                               00543800
//*                                                                     00543900
//UNCATL2  EXEC PGM=IEFBR14                                             00544002
//DD1       DD DSN=GAOP.PPP5622.$PCRCYM,                                00544102
//             DISP=(MOD,DELETE,DELETE),                                00544202
//             SPACE=(TRK,(0)),                                         00544302
//             UNIT=SYSDA                                               00544402
//*                                                                     00544502
//*UNCATL3  EXEC PGM=IEFBR14                                            00544602
//*DD1       DD DSN=GAOP.PLRS600.$PCRCYM,                               00544702
//*             DISP=(MOD,DELETE,DELETE),                               00544802
//*             SPACE=(TRK,(0)),                                        00544902
//*             UNIT=SYSDA                                              00545002
//*                                                                     00545102
//UNCATL4  EXEC PGM=IEFBR14                                             00545202
//DD1       DD DSN=GAOP.PLRS601.$PCRCYM,                                00545302
//             DISP=(MOD,DELETE,DELETE),                                00545402
//             SPACE=(TRK,(0)),                                         00545502
//             UNIT=SYSDA                                               00545602
//*                                                                     00545702
//UNCATL5  EXEC PGM=IEFBR14                                             00545802
//DD1       DD DSN=GAOP.PLRS602.$PCRCYM,                                00545902
//             DISP=(MOD,DELETE,DELETE),                                00546002
//             SPACE=(TRK,(0)),                                         00546102
//             UNIT=SYSDA                                               00546202
//*                                                                     00546302
//UNCATL6  EXEC PGM=IEFBR14                                             00546402
//DD1       DD DSN=GAOP.PLRS603.$PCRCYM,                                00546502
//             DISP=(MOD,DELETE,DELETE),                                00546602
//             SPACE=(TRK,(0)),                                         00546702
//             UNIT=SYSDA                                               00546802
//*                                                                     00546902
//UNCATL7  EXEC PGM=IEFBR14                                             00547002
//DD1       DD DSN=GAOP.PPP5623.$PCRCYM,                                00547102
//             DISP=(MOD,DELETE,DELETE),                                00547202
//             SPACE=(TRK,(0)),                                         00547302
//             UNIT=SYSDA                                               00547402
//*                                                                     00547502
//UNCATL8  EXEC PGM=IEFBR14                                             00547602
//DD1       DD DSN=GAOP.PPP563N.$PCRCYM,                                00547702
//             DISP=(MOD,DELETE,DELETE),                                00547802
//             SPACE=(TRK,(0)),                                         00547902
//             UNIT=SYSDA                                               00548002
//*                                                                     00548102
//********* COPY FILES FOR OTHER SYSTEMS ****************************** 00548202
//********** COPY PPP490 GTN081 TAPEFILE ***************8               00548400
//*                                                                     00548500
//COPYL1   EXEC PGM=SYNCSORT                                            00548600
//SORTIN    DD DSN=PPSP.TAPEFILE.GTN081,                                00548700
//             DISP=SHR                                                 00548800
//SORTOUT   DD DSN=FISP.PPP490.GTN081.$PCRCYM,                          00548900
//             DISP=(,CATLG,DELETE),                                    00549000
//*            DCB=(RECFM=FB,LRECL=80,BLKSIZE=22960),                   00549100
//             SPACE=(TRK,(15,75),RLSE),                                00549200
//             UNIT=SYSDA,                                              00549300
//             VOL=SER=APC005                                           00549400
//SORTWK01  DD SPACE=(TRK,30),                                          00549500
//             UNIT=SYSDA                                               00549600
//SORTWK02  DD SPACE=(TRK,30),                                          00549700
//             UNIT=SYSDA                                               00549800
//SORTWK03  DD SPACE=(TRK,30),                                          00549900
//             UNIT=SYSDA                                               00550000
//SYSOUT    DD SYSOUT=*                                                 00550100
//SYSIN     DD DSN=PPSP.PARMLIB(COPYFILE),                              00550200
//             DISP=SHR                                                 00550300
//*                                                                     00550400
//********* COPY FILES FOR OTHER SYSTEMS ****************************** 00550500
//*                                                                     00550700
//COPYL2   EXEC PGM=SYNCSORT                                            00550800
//SORTIN   DD DSN=PPSP.PPP5622,                                         00550900
//             DISP=SHR                                                 00551000
//SORTOUT   DD DSN=GAOP.PPP5622.$PCRCYM,                                00551100
//             DISP=(,CATLG,DELETE),                                    00551200
//*            DCB=(RECFM=FB,LRECL=133,BLKSIZE=22876),                  00551300
//             SPACE=(TRK,(250,75),RLSE),                               00551400
//             UNIT=SYSDA                                               00551500
//SORTWK01  DD SPACE=(TRK,30),                                          00551600
//             UNIT=SYSDA                                               00551700
//SORTWK02  DD SPACE=(TRK,30),                                          00551800
//             UNIT=SYSDA                                               00551900
//SORTWK03  DD SPACE=(TRK,30),                                          00552000
//             UNIT=SYSDA                                               00552100
//SYSOUT    DD SYSOUT=*                                                 00552200
//SYSIN     DD DSN=PPSP.PARMLIB(COPYFILE),                              00552300
//             DISP=SHR                                                 00552400
//*                                                                     00552500
//*                                                                     00552602
//*=========== COMMENTED OUT 10/02/2014 NO LONGER NEEDED ======         00552602
//*COPYL3   EXEC PGM=SYNCSORT                                           00552702
//*SORTIN   DD DSN=PPSP.PLRS600.$PCRCYM,                                00552802
//*            DISP=SHR                                                 00552902
//*SORTOUT  DD DSN=GAOP.PLRS600.$PCRCYM,                                00553002
//*            DISP=(,CATLG,DELETE),                                    00553102
//*            DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 00553202
//*            SPACE=(TRK,(60,30),RLSE),                                00553302
//*            UNIT=SYSDA                                               00553402
//*SORTWK01 DD SPACE=(TRK,30),                                          00553502
//*            UNIT=SYSDA                                               00553602
//*SORTWK02 DD SPACE=(TRK,30),                                          00553702
//*            UNIT=SYSDA                                               00553802
//*SORTWK03 DD SPACE=(TRK,30),                                          00553902
//*            UNIT=SYSDA                                               00554002
//*SYSOUT   DD SYSOUT=*                                                 00554102
//*SYSIN    DD DSN=PPSP.PARMLIB(COPYFILE),                              00554202
//*            DISP=SHR                                                 00554302
//*==================================================                   00552602
//COPYXMED EXEC PGM=SYNCSORT                                            00552702
//SORTIN   DD DSN=PPSP.XLSMED.$PMMYR,                                   00552802
//            DISP=SHR                                                  00552902
//SORTOUT  DD DSN=GAOP.XLSMED.$PMMYR,                                   00553002
//            DISP=(,CATLG,DELETE),                                     00553102
//            DCB=(RECFM=FB,LRECL=335,BLKSIZE=27805),                   00553202
//            SPACE=(TRK,(60,30),RLSE),                                 00553302
//            UNIT=SYSDA                                                00553402
//SORTWK01 DD SPACE=(TRK,30),                                           00553502
//            UNIT=SYSDA                                                00553602
//SORTWK02 DD SPACE=(TRK,30),                                           00553702
//            UNIT=SYSDA                                                00553802
//SORTWK03 DD SPACE=(TRK,30),                                           00553902
//            UNIT=SYSDA                                                00554002
//SYSOUT   DD SYSOUT=*                                                  00554102
//SYSIN    DD DSN=PPSP.PARMLIB(COPYFILE),                               00554202
//            DISP=SHR                                                  00554302
//*=================================================                    00554402
//*                                                                     00554502
//COPYL4   EXEC PGM=SYNCSORT                                            00554602
//SORTIN   DD DSN=PPSP.PLRS601.$PCRCYM,                                 00554702
//             DISP=SHR                                                 00554802
//SORTOUT   DD DSN=GAOP.PLRS601.$PCRCYM,                                00554902
//             DISP=(,CATLG,DELETE),                                    00555002
//*            DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 00555102
//             SPACE=(TRK,(60,30),RLSE),                                00555202
//             UNIT=SYSDA                                               00555302
//SORTWK01  DD SPACE=(TRK,30),                                          00555402
//             UNIT=SYSDA                                               00555502
//SORTWK02  DD SPACE=(TRK,30),                                          00555602
//             UNIT=SYSDA                                               00555702
//SORTWK03  DD SPACE=(TRK,30),                                          00555802
//             UNIT=SYSDA                                               00555902
//SYSOUT    DD SYSOUT=*                                                 00556002
//SYSIN     DD DSN=PPSP.PARMLIB(COPYFILE),                              00556102
//             DISP=SHR                                                 00556202
//*                                                                     00556302
//COPYL5   EXEC PGM=SYNCSORT                                            00556402
//SORTIN   DD DSN=PPSP.PLRS602.$PCRCYM,                                 00556502
//             DISP=SHR                                                 00556602
//SORTOUT   DD DSN=GAOP.PLRS602.$PCRCYM,                                00556702
//             DISP=(,CATLG,DELETE),                                    00556802
//*            DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 00556902
//             SPACE=(TRK,(60,30),RLSE),                                00557002
//             UNIT=SYSDA                                               00557102
//SORTWK01  DD SPACE=(TRK,30),                                          00557202
//             UNIT=SYSDA                                               00557302
//SORTWK02  DD SPACE=(TRK,30),                                          00557402
//             UNIT=SYSDA                                               00557502
//SORTWK03  DD SPACE=(TRK,30),                                          00557602
//             UNIT=SYSDA                                               00557702
//SYSOUT    DD SYSOUT=*                                                 00557802
//SYSIN     DD DSN=PPSP.PARMLIB(COPYFILE),                              00557902
//             DISP=SHR                                                 00558002
//*                                                                     00558102
//*                                                                     00558202
//COPYL6   EXEC PGM=SYNCSORT                                            00558302
//SORTIN   DD DSN=PPSP.PLRS603.$PCRCYM,                                 00558402
//             DISP=SHR                                                 00558502
//SORTOUT   DD DSN=GAOP.PLRS603.$PCRCYM,                                00558602
//             DISP=(,CATLG,DELETE),                                    00558702
//*            DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 00558802
//             SPACE=(TRK,(60,30),RLSE),                                00558902
//             UNIT=SYSDA                                               00559002
//SORTWK01  DD SPACE=(TRK,30),                                          00559102
//             UNIT=SYSDA                                               00559202
//SORTWK02  DD SPACE=(TRK,30),                                          00559302
//             UNIT=SYSDA                                               00559402
//SORTWK03  DD SPACE=(TRK,30),                                          00559502
//             UNIT=SYSDA                                               00559602
//SYSOUT    DD SYSOUT=*                                                 00559702
//SYSIN     DD DSN=PPSP.PARMLIB(COPYFILE),                              00559802
//             DISP=SHR                                                 00559902
//*                                                                     00560002
//*                                                                     00560102
//COPYL7   EXEC PGM=SYNCSORT                                            00560202
//SORTIN   DD DSN=PPSP.PPP5623.$PCRCYM,                                 00560302
//             DISP=SHR                                                 00560402
//SORTOUT   DD DSN=GAOP.PPP5623.$PCRCYM,                                00560502
//             DISP=(,CATLG,DELETE),                                    00560602
//*            DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 00560702
//             SPACE=(TRK,(60,30),RLSE),                                00560802
//             UNIT=SYSDA                                               00560902
//SORTWK01  DD SPACE=(TRK,30),                                          00561002
//             UNIT=SYSDA                                               00561102
//SORTWK02  DD SPACE=(TRK,30),                                          00561202
//             UNIT=SYSDA                                               00561302
//SORTWK03  DD SPACE=(TRK,30),                                          00561402
//             UNIT=SYSDA                                               00561502
//SYSOUT    DD SYSOUT=*                                                 00561602
//SYSIN     DD DSN=PPSP.PARMLIB(COPYFILE),                              00561702
//             DISP=SHR                                                 00561802
//*                                                                     00561902
//*                                                                     00562002
//COPYL8   EXEC PGM=SYNCSORT                                            00562102
//SORTIN   DD DSN=PPSP.PPP563N.$PCRCYM,                                 00562202
//             DISP=SHR                                                 00562302
//SORTOUT   DD DSN=GAOP.PPP563N.$PCRCYM,                                00562402
//             DISP=(,CATLG,DELETE),                                    00562502
//*            DCB=(RECFM=FBA,LRECL=133,BLKSIZE=27930),                 00562602
//             SPACE=(TRK,(175,750),RLSE),                              00562702
//             UNIT=SYSDA                                               00562802
//SORTWK01  DD SPACE=(TRK,30),                                          00562902
//             UNIT=SYSDA                                               00563002
//SORTWK02  DD SPACE=(TRK,30),                                          00563102
//             UNIT=SYSDA                                               00563202
//SORTWK03  DD SPACE=(TRK,30),                                          00563302
//             UNIT=SYSDA                                               00563402
//SYSOUT    DD SYSOUT=*                                                 00563502
//SYSIN     DD DSN=PPSP.PARMLIB(COPYFILE),                              00563602
//             DISP=SHR                                                 00563702
//*                                                                     00563802
//*========================================================             00563902
//*        EMAIL NOTIFICATION TO BFS                                    00564002
//*========================================================             00564102
//MAILIT EXEC PGM=IKJEFT01,DYNAMNBR=20                                  00564202
//SYSPROC DD DSN=SYST.T.CLIST,DISP=SHR                                  00564302
//SYSPRINT DD SYSOUT=*                                                  00564402
//SYSTSPRT DD SYSOUT=*                                                  00564502
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:
//DESTIDS DD *
DMESERVE@UCSD.EDU
/*
//* EMAIL TEXT FOLLOWS:
//NOTES DD *
REMINDER:

The following files have been created on the Mainframe and are
available for ftp to BFS:

'GAOP.PPP5622.$PCRCYM'  (BENEFITS DEDUCTION REGISTER)
'GAOP.PLRS601.$PCRCYM'  (PAYROLL RECONCILIATION REPORT #2)
'GAOP.PLRS602.$PCRCYM'  (PAYROLL RECONCILIATION REPORT #3)
'GAOP.PLRS603.$PCRCYM'  (PAYROLL RECONCILIATION REPORT #4)
'GAOP.PPP5623.$PCRCYM'  (SELF BILLING STATEMENTS)
'GAOP.PPP563N.$PCRCYM'  (UNMATCHED CURRENT MONTH ACTIVITY)

 THANK YOU,
 MGR, PRODUCTION CONTROL
 EMAIL: ACT-PRODCONTROL@UCSD.EDU


/*
//* CONTROL REPORT FILENAME FOLLOWS:
//SYSTSIN   DD *
 %MAILRPT -
 'PPSP.DUMMY.MAIL.MESSAGE' -
 SUBJECT('IMAGING FILE FOR BFS')
/*
//*---
//* THE 'DUMMY' MESSAGE MUST BE USED ABOVE...
//*============================================================
//*  FTP TO BFS
//*============================================================
//SUBBFSFT EXEC PGM=ZEKESET,
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))
//SYSPRINT  DD SYSOUT=*
//SYSIN     DD *
 SET SCOM 'ZADD EV (2126 2147 2203 2229 2280 2379 3662) REBUILD'
/*
//*
//*============================================================         00569702
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00569802
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00569902
//*============================================================         00570002
//STOPZEKE EXEC STOPZEKE                                                00570102
//*                                                                     00571002
//*================ E N D  O F  J C L  PPSPM620 ========                00580002
