//PPSPA105 JOB SYS000,'PAUL R - PAYROLL',MSGLEVEL=(1,1),                00000100
//        NOTIFY=APCDBM,CLASS=F,MSGCLASS=X,                             00000200
//*====================================================                 00110000
//*        RESTART=(STEP3O),                                            00000400
//*====================================================                 00110000
//        REGION=20M                                                    00000600
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 1135
//*++++++++++++++++++++++++++
//*    *N.T.E.*
//*++++++++++++++++++++++++++
//*====================================================                 00110000
//*  5/6/99 - AS OF REL1230, ACCEPTABLE CONDITION CODES
//*     FOR THIS JOB ARE 00 - 07
//*     *** 08 AND GREATER ARE NOT ACCEPTABLE ***
//*====================================================                 00110000
//JOBLIB    DD DSN=PPSP.LOADLIB,                                        00001000
//             DISP=SHR                                                 00001100
//          DD DSN=DB2P.DCA.LOAD,                                       00001200
//             DISP=SHR                                                 00001300
//          DD DSN=DB2P.DSNEXIT,                                        00001400
//             DISP=SHR                                                 00001500
//          DD DSN=DB2P.DSNLOAD,                                        00001600
//             DISP=SHR                                                 00001700
//          DD DSN=CEE.SCEERUN,                                         00001600
//             DISP=SHR                                                 00001700
//*********************************************************************
//* FOLLOWING 2 LINES INACTIVATED ON 6/9/2012 BY REQUEST OF LEV.
//*********************************************************************
//*         DD DSN=SYS2.COB2.COB2LIB,                                   00001600
//*            DISP=SHR                                                 00001700
//*============================================================         00110000
//**  THIS JOB IS THE FIRST JOB OF 2 FOR THE COA DAILY EXTRACT.
//**  IT SHOULD RUN IN THE 480 ENVIRONMENT AND IS FOLLOWED BY
//**  JOB PPSPA106 (580).
//**
//**  THIS JOB SHOULD BE RUN AFTER ANY REGULARLY SCHEDULED
//**  PAYROLL PROCESSING IS FINISHED, FOR EXAMPLE, AFTER:
//**      PAYCYCLE (PPSPP910)
//**      DAILY    (PAYDPURG)
//**      MONTHEND (PPSPM940)
//**      PTR      (PPSPP912)
//**  AND AFTER THE FINANCIAL CHART UPDATE (CHD401AN) IS DONE
//**  *THIS IS AN EXTRACT PROCESS ONLY AND CAN RUN INTO THE DAY -
//**  *DAILY JOB PPSP.JCL.CNTL(PAY86) WILL LOAD THE EXTRACT FILES
//**  *TO THE COA DB2 TABLES THE NEXT EVENING
//**
//**  PPSP.PARMLIB(AS30S2) SHOULD BE EDITED FOLLOWING
//**  THE COMPLETION OF PPSPM940 TO REFLECT THE END OF
//**  THE MONTH OF THE 'NEXT PROCESSING MONTH'
//**
//**  IE.  IF YOU ARE RUNNING M9202 MONTHEND, THEN
//**       AS30S2 DATES SHOULD BE CHANGED TO '19920331',
//**       FORMAT IS YYYYMMDD, WHEN JOB PPSPM940 IS FINISHED.
//**
//**  ALL STEPS SHOULD HAVE CONDITION CODE 00
//**
//*====================================================                 00110000
// JCLLIB ORDER=APCP.PROCLIB                                            00053000
//OUT1 OUTPUT CLASS=X,FORMS=PA11,JESDS=ALL,DEFAULT=YES,DEST=RMT99
//*
//*====================================================                 00110000
//*   SORT INDEX EXTRACT FILE
//*====================================================                 00110000
//PPSPAS30 EXEC PPSPAS30,
//             COND=((7,LT))                                            00000500
//STEP2.SYSIN DD DSN=PPSP.PARMLIB(AS30S1),DISP=SHR
//            DD DSN=PPSP.PARMLIB(AS30S2),DISP=SHR
//            DD DSN=PPSP.PARMLIB(AS30S3),DISP=SHR
//*
//*====================================================                 00110000
//*   SORT FUND EXTRACT FILE
//*====================================================                 00110000
//PPSPAS31 EXEC PPSPAS31,
//             COND=((7,LT))                                            00000500
//STEP2.SYSIN DD DSN=PPSP.PARMLIB(AS31S1),DISP=SHR
//            DD DSN=PPSP.PARMLIB(AS30S2),DISP=SHR
//            DD DSN=PPSP.PARMLIB(AS31S2),DISP=SHR
//*
//*====================================================                 00110000
//*   SORT ORG EXTRACT FILE
//*
//* NOTE: STEPS STEP3O-STEP6O OVERRIDE CERTAIN
//*       FINANCIAL MANAGER MAILDROP CODES FOR
//*       PEDIATRICS AND SURGERY, ETC.
//*====================================================                 00110000
//*
//STEP1O   EXEC PGM=IEFBR14,                                            00080000
//             COND=((7,LT))                                            00000500
//*            REGION=300K                                              00090000
//DD1       DD DSN=PPSP.COA.SORTORG.ORIG,                               00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//*                                                                     00100000
//STEP2O   EXEC PGM=SYNCSORT,                                           00110000
//             COND=((7,LT))                                            00000500
//SORTWK01 DD UNIT=SYSDA,SPACE=(TRK,(30))                               00120000
//SORTWK02 DD UNIT=SYSDA,SPACE=(TRK,(30))                               00130000
//SORTWK03 DD UNIT=SYSDA,SPACE=(TRK,(30))                               00140000
//SORTWK04 DD UNIT=SYSDA,SPACE=(TRK,(30))                               00150000
//SYSOUT   DD SYSOUT=*                                                  00160000
//SORTIN   DD DSN=GUEST.IFIS.ORG,DISP=SHR                               00170000
//SORTOUT  DD DSN=PPSP.COA.SORTORG.ORIG,                                00180000
//            DISP=(,CATLG,DELETE),                                     00190000
//            DCB=(RECFM=FB,LRECL=143,BLKSIZE=14300),                   00200000
//            SPACE=(TRK,(30,15),RLSE),                                 00210000
//            UNIT=SYSDA,                                               00220000
//            VOL=SER=APC002,                                           00230000
//            MGMTCLAS=AS
//SYSIN    DD DSN=PPSP.PARMLIB(AS32S1),DISP=SHR                         00250000
//         DD DSN=PPSP.PARMLIB(AS30S2),DISP=SHR                         00250000
//         DD DSN=PPSP.PARMLIB(AS32S2),DISP=SHR                         00250000
//*                                                                     00250000
//STEP3O   EXEC PGM=IEFBR14,                                            00080000
//             COND=((7,LT))                                            00000500
//*            REGION=300K                                              00090000
//DD1       DD DSN=PPSP.COA.SORTORG.T1,                                 00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD2       DD DSN=PPSP.COA.SORTORG.T2,                                 00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//DD3       DD DSN=PPSP.COA.SORTORG,                                    00100000
//             DISP=(MOD,DELETE,DELETE),                                00110000
//             SPACE=(TRK,(0)),                                         00120000
//             UNIT=SYSDA                                               00130000
//*                                                                     00100000
//STEP4O   EXEC PGM=PPL802,
//             COND=((7,LT))                                            00000500
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//CNTLCARD DD *
RECSIZE    143 160
TARGET   6   1   1
RESULT  30  68 116
DEFAULT  OFF
/*
//TABLEFIL DD DSN=PPSP.COA.ORG.FMGR.EXCEPT,DISP=SHR
//DATAFILE DD DSN=PPSP.COA.SORTORG.ORIG,DISP=SHR
//PRINTER  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//SYSDBOUT DD SYSOUT=*
//SYSABEND DD SYSOUT=*
//ABENDAID DD SYSOUT=*
//OUTPUT   DD DSN=PPSP.COA.SORTORG.T1,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=500,BLKSIZE=23000),
//            SPACE=(TRK,(300,150),RLSE),
//            VOL=SER=APC003,
//            DISP=(NEW,CATLG,DELETE)
//REPORT   DD SYSOUT=*
//CNTLRPT  DD SYSOUT=*
//*
//STEP5O   EXEC PGM=PPL802,
//             COND=((7,LT))                                            00000500
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//CNTLCARD DD *
RECSIZE    143 160
TARGET   6   1   1
RESULT  15  98 146
DEFAULT  OFF
/*
//TABLEFIL DD DSN=PPSP.COA.ORG.FMGR.EXCEPT,DISP=SHR
//DATAFILE DD DSN=PPSP.COA.SORTORG.T1,DISP=SHR
//PRINTER  DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSABOUT DD SYSOUT=*
//SYSDBOUT DD SYSOUT=*
//SYSABEND DD SYSOUT=*
//ABENDAID DD SYSOUT=*
//OUTPUT   DD DSN=PPSP.COA.SORTORG.T2,
//            UNIT=SYSDA,
//            DCB=(RECFM=FB,LRECL=500,BLKSIZE=23000),
//            SPACE=(TRK,(300,150),RLSE),
//            VOL=SER=APC003,
//            DISP=(NEW,CATLG,CATLG)
//REPORT   DD SYSOUT=*
//CNTLRPT  DD SYSOUT=*
//*
//STEP6O   EXEC PGM=SYNCSORT,
//             COND=((7,LT))                                            00000500
//SYSOUT   DD SYSOUT=*
//SORTWK01 DD UNIT=SYSDA,SPACE=(CYL,30)
//SORTIN   DD DSN=PPSP.COA.SORTORG.T2,
//            DISP=SHR
//SORTOUT  DD DSN=PPSP.COA.SORTORG,
//            DISP=(NEW,CATLG,CATLG),
//            DCB=(RECFM=FB,LRECL=143,BLKSIZE=27885),
//            SPACE=(TRK,(30,15),RLSE),
//            VOL=SER=APC003,
//            UNIT=SYSDA
//SYSIN    DD *
  SORT FIELDS=(1,6,CH,A)
  OUTREC FIELDS=(1,143)
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEK4 EXEC STOPZEK4
//*
//*================ E N D  O F  J C L  PPSPA105 ========
