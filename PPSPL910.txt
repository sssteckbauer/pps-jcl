//PPSPL910 JOB SYS000,'PRDCTL',MSGLEVEL=(1,1),
//        NOTIFY=APCPSB,CLASS=F,REGION=0M,
//*====================================================                 00110000
//*    RESTART=(STEP1),
//*====================================================                 00110000
//        MSGCLASS=F
//***************************
//*    ZEKE EVENT # 1814
//***************************
//*====================================================                 00110000
//*  5/6/99 - AS OF REL1230, ACCEPTABLE CONDITION CODES
//*     FOR THIS JOB ARE 00 - 07
//*     *** 08 AND GREATER ARE NOT ACCEPTABLE ***
//*  5/11/15- REMOVE UNCATALOG OF PPSP.LVP.$PCRCYM..REPORT.TB
//*           TO KEEP AROUND FOR FOR BOB M RESEARCH OF LVP FOR ACA.
//*====================================================                 00110000
//OUT1 OUTPUT CLASS=F,FORMS=PA11,JESDS=ALL,DEFAULT=YES,GROUPID=PRDCTL
//*ROUTE PRINT RMT10
// JCLLIB ORDER=APCP.PROCLIB                                            00053000
//*
//**************** UNCATALOG *****************************************  00530017
//*                                                                     00540017
//STEP1   EXEC PGM=IEFBR14                                              00550019
//*D1       DD DSN=PPSP.LVP.$PCRCYM..REPORT.TB,                         00570027
//*            DISP=(MOD,DELETE,DELETE),                                00580017
//*            SPACE=(TRK,(0)),                                         00590017
//*            UNIT=SYSDA                                               00600017
//DD2       DD DSN=PPSP.LVP.$PCRCYM..REPORT.T0,                         00570027
//             DISP=(MOD,DELETE,DELETE),                                00580017
//             SPACE=(TRK,(0)),                                         00590017
//             UNIT=SYSDA                                               00600017
//DD3       DD DSN=PPSP.LVP.$PCRCYM..REPORT.T1,                         00570027
//             DISP=(MOD,DELETE,DELETE),                                00580017
//             SPACE=(TRK,(0)),                                         00590017
//             UNIT=SYSDA                                               00600017
//DD4       DD DSN=PPSP.LVP.$PCRCYM..REPORT.T2,                         00570027
//             DISP=(MOD,DELETE,DELETE),                                00580017
//             SPACE=(TRK,(0)),                                         00590017
//             UNIT=SYSDA                                               00600017
//DD5       DD DSN=PPSP.LVP.$PCRCYM..REPORT.T3,                         00570027
//             DISP=(MOD,DELETE,DELETE),                                00580017
//             SPACE=(TRK,(0)),                                         00590017
//             UNIT=SYSDA                                               00600017
//DD6       DD DSN=PPSP.LVP.$PCRCYM..SKLZERO.EXTEN.PPPEAR.T1,           00570027
//             DISP=(MOD,DELETE,DELETE),                                00580017
//             SPACE=(TRK,(0)),                                         00590017
//             UNIT=SYSDA                                               00600017
//DD7       DD DSN=PPSP.LVP.$PCRCYM..SKLZERO.EXTEN.UPDATED.T1,          00570027
//             DISP=(MOD,DELETE,DELETE),                                00580017
//             SPACE=(TRK,(0)),                                         00590017
//             UNIT=SYSDA                                               00600017
//DD8       DD DSN=PPSP.LVP.$PCRCYM..SKLZERO.XEMX.T1,                   00570027
//             DISP=(MOD,DELETE,DELETE),                                00580017
//             SPACE=(TRK,(0)),                                         00590017
//             UNIT=SYSDA                                               00600017
//DD9       DD DSN=PPSP.LVP.$PCRCYM..SKLZERO.XEMX.T2,                   00570027
//             DISP=(MOD,DELETE,DELETE),                                00580017
//             SPACE=(TRK,(0)),                                         00590017
//             UNIT=SYSDA                                               00600017
//*                                                                     00630025
//*====================================================                 00110000
//* UNCATALOGUE ALL "INPUT" PDS SNAPSHOT DB2 DATASETS
//*====================================================                 00110000
//PPSPPC50 EXEC PPSPPC50,CCYYMM='$PCRCYM',
//             COND=((7,LT))
//*
//*====================================================                 00110000
//* UNCATALOGUE ALL "INPUT" SEQUENTIAL EDB DB2 DATASETS
//*====================================================                 00110000
//PPSPPC80 EXEC PPSPPC80,PDSTYPE='INPUT',CCYYMM='$PCRCYM',
//             COND=((7,LT))
//*
//*====================================================                 00110000
//* UNCATALOGUE ALL "INPUT" SEQUENTIAL COA DB2 DATASETS
//*====================================================                 00110000
//PPSPPC90 EXEC PPSPPC90,PDSTYPE='INPUT',CCYYMM='$PCRCYM',
//             COND=((7,LT))
//*
//*====================================================                 00110000
//* UNCATALOGUE ALL "INPUT" SEQUENTIAL CTL DB2 DATASETS
//*====================================================                 00110000
//PPSPPC55 EXEC PPSPPC55,PDSTYPE='INPUT',CCYYMM='$PCRCYM',
//             COND=((7,LT))
//*
//*=============================================================
//*   RESETS ZEKE VARIABLE TO 'STOP' TO CONTROL LEAVE PROCESSING
//*   FOR NEXT MONTH. VARIABLE MANUALLY SET TO 'GO' AFTER
//*   CHECKING TOTAL OF 'PPSP.LVTRANS.CUMM' EACH MONTH AT
//*   MONTHEND PAYROLL PROCESSING (VERIFICATION SHEETS).
//*=============================================================
//SETS2 EXEC PGM=ZEKESET
//SYSPRINT DD SYSOUT=*
//SYSIN DD *
  SET VAR $PPLEAVE EQ 'STOP'
/*
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEK4 EXEC STOPZEK4
//*
//*================ E N D  O F  J C L  PPSPL910 ========
