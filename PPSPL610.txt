//PPSPL610 JOB SYS000,'PAYROLL',MSGLEVEL=(1,1),                         00010000
//        NOTIFY=APCPSB,CLASS=F,REGION=0M,                              00020000
//*====================================================                 00030000
//*   RESTART=(PPL7011),                                                00040000
//*====================================================                 00050000
//        MSGCLASS=F                                                    00060000
//*       COND=(5,LT)                                                   00070000
//*++++++++++++++++++++++++++                                           00071000
//*    ZEKE EVENT # 1812                                                00072000
//*++++++++++++++++++++++++++                                           00073000
//*                                                                     00074000
// JCLLIB ORDER=APCP.PROCLIB                                            00075000
/*JOBPARM  LINES=1250                                                   00076000
//OUTPL610 OUTPUT JESDS=ALL,DEFAULT=YES,FORMS=PA11,DEST=LOCAL           00077000
//RMT7OUT  OUTPUT CLASS=X,FORMS=PA01,DEST=LOCAL,GROUPID=PAYROLL         00080000
//RMT7BAK  OUTPUT CLASS=G,FORMS=PA01,DEST=LOCAL,GROUPID=BACKUP2         00090000
//*                                                                     00100000
//******************************************************************
//PPL8711  EXEC PGM=IEBGENER,REGION=600K
//SYSUT1   DD DSN=PPSP.LVP.$PCRCYM..PPL8711,DISP=SHR
//SYSUT2   DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSIN    DD DUMMY
//*
//*****************************************************************
//PPL7011  EXEC PGM=IEBGENER,REGION=600K
//SYSUT1   DD DSN=PPSP.LVP.$PCRCYM..PPL7011,DISP=SHR
//SYSUT2   DD SYSOUT=*
//SYSOUT   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSIN    DD DUMMY
//*
//**********************************************************
//PPL8781.FICHE EXEC PGM=IEBGENER
//SYSUT1   DD DSN=PPSP.LVP.$PCRCYM..PPL8781.FICHE,DISP=SHR
//SYSUT2   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSIN    DD DUMMY
//SYSOUT   DD SYSOUT=*
//*
//**********************************************************
//PPL8781  EXEC PGM=IEBGENER
//SYSUT1   DD DSN=PPSP.LVP.$PCRCYM..PPL8781.PRINT,DISP=SHR
//SYSUT2   DD SYSOUT=*
//SYSUDUMP DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSIN    DD DUMMY
//SYSOUT   DD SYSOUT=*
//*
//*============================================================         00721000
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00730000
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00740000
//*============================================================         00750000
//STOPZEKE EXEC STOPZEKE                                                00760000
//*                                                                     00770000
//*================ E N D  O F  J C L  PPSPL610 ========                00780000
