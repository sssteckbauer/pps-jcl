//TESTMAIL JOB (SYS000),'PAUL - S003',MSGLEVEL=(1,1),MSGCLASS=F,        00010001
//*    RESTART=MAILIT,
//         NOTIFY=APCDBM,CLASS=F,REGION=20M                             00020001
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 3474
//*++++++++++++++++++++++++++
//*====================================================                 00110000
//OUT1 OUTPUT CLASS=F,FORMS=PA11,JESDS=ALL,DEFAULT=YES,GROUPID=PRDCTL
//LIB  JCLLIB ORDER=(APCP.PROCLIB,FISP.PARMLIB)
//*====================================================                 00110000
//*   SEND MAIL MESSAGES
//*=================================================                    00004200
//MAILIT EXEC PGM=IKJEFT01,DYNAMNBR=20
//SYSPROC DD DSN=SYST.T.CLIST,DISP=SHR
//SYSPRINT DD SYSOUT=*
//SYSTSPRT DD SYSOUT=F
//* MAIL NOTICES BEING SENT TO THE FOLLOWING PEOPLE:
//DESTIDS DD *
DMESERVE@UCSD.EDU
/*
//* EMAIL TEXT FOLLOWS:
//NOTES DD *
** this is still a test message... testing the mail system **

A UCSD PAYROLL FILE (CLASS KH001) HAS JUST BEEN PLACED IN THE

ADVANTIS MAILBOX.  PLEASE PULL THE FILE OUT FOR PROCESSING

AT YOUR EARLIEST CONVENIENCE.

/*
//* CONTROL REPORT FILENAME FOLLOWS
//SYSTSIN   DD *
 %MAILRPT -
 'FISP.DUMMY.MAIL.MESSAGE' -
 SUBJECT('UCSD PAYROLL FILE IS AVAILABLE')
/*
//*---
//* THE 'DUMMY' MESSAGE MUST BE USED ABOVE...
//*---
//*============================================================         00005200
//*
//*================ E N D  O F  J C L  ZZ1 ========                     00250009
