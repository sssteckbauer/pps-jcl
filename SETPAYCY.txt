//SETPAYCY  JOB (SYS000),'PC 0903',MSGLEVEL=(1,1),                      00010099
//     NOTIFY=APCDBM,MSGCLASS=F,CLASS=0                                 00020086
//*=======================================================              00020199
//*  NO ZEKE PANEL-SIMPLE SUB                                           00020299
//*=======================================================              00020399
//*   REMEMBER TO EDIT THE PARMS IN 'SETNEXTC' JOB                      00020499
//*   (SETNEXTC SETS TH55VARIABLE OF WORKSTUDYCAP FOR                   00020599
//*    'NEXT' BIWEEKLY PAYROLL PROCESSING-ONLY)                         00020699
//*=======================================================              00020799
/*ROUTE PRINT RMT99                                                     00021099
//*=======================================================              00021299
//Z1     EXEC PGM=ZEKESET,                                              00022099
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                       00022100
//SYSPRINT DD SYSOUT=*                                                  00022200
//SYSIN   DD *                                                          00022399
  SET VAR $PPTRSPC     EQ EX                                            00022499
  SET VAR $PCRCYM      EQ 'X62012'                                      00022599
  SET VAR $PCHECKS     EQ NO                                            00022799
  SET VAR $PPETCOH     EQ NO                                            00022899
  SET VAR $P1CYCLE     EQ NO                                            00023099
  SET VAR $PCURRCC     EQ X6                                            00023199
  SET VAR $PCURRCCUCOP EQ EX                                            00023299
  SET VAR $PCYMOUCOP   EQ 'EX.DEC'                                      00023399
  SET VAR $PPSXFER     EQ $PCRCYM                                       00023899
/*                                                                      00023999
//*****************************************************                 00024099
//*  $PCURRCCUCOP - UCOP'S NAMING CONVENTION FOR THE                    00024199
//*  FILE THAT WILL BE FTP'D THROUGH 'PUTOPC1'.                         00024299
//* (XX CYCLES)                                                         00024399
//*  UCSD: 'PPSP.PAYTRANS.$PCRCYM'                                      00024499
//*  UCOP: 'PP6.PP350.EX.USR'                                           00024599
//*-----------------------------                                        00024699
//* (B3 CYCLES) (SECOND B1) (B1(3)0507)                                 00024799
//*  UCSD: 'PPSP.PAYTRANS.B30507' ($PCRCYM)                             00024899
//*  UCOP: 'PP6.PP350.B1TWO.USR'                                        00024999
//*                                                                     00025099
//* (B3 CYCLES) (SECOND B2) (B2(3)0501)                                 00025199
//*  UCSD: 'PPSP.PAYTRANS.B30501' ($PCRCYM)                             00025299
//*  UCOP: 'PP6.PP350.B2TWO.USR'                                        00025399
//*                                                                     00025499
//* NORMALLY MATCHES VALUE OF $PCURRCC EXCEPT DURING                    00025599
//* SPECIAL COMPUTE PROCESSES SUCH AS 'XX' OR 'B3' CYCLES.              00025699
//*                                                                     00025799
//*****************************************************                 00025899
//Z2     EXEC ZEKEUTLP,                                                 00025999
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                       00026099
//SYSPRINT DD SYSOUT=*                                                  00026199
//SYSIN    DD *                                                         00026299
   LIST VARIABLES                                                       00026399
/*                                                                      00026499
//*============================================================         00026599
//*    ZADD'S THE JOB 'SETNEXTC' THAT WILL SET THE VARIABLES            00026699
//*    FOR THE "NEXT" BIWEEKLY COMPUTE PROCESS OF WORKSTUDY CAP         00026799
//*============================================================         00026899
//*SUBSETNT EXEC PGM=ZEKESET,                                           00026999
//*             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                      00027099
//*SYSPRINT  DD SYSOUT=*                                                00027199
//*SYSIN     DD *                                                       00027299
//* SET SCOM 'ZADD EV (1701) REBUILD'                                   00027399
//*                                                                     00027499
//*============================================================         00027599
//Z3     EXEC PGM=ZEKESET,                                              00027799
//             COND=((9,LT),(5,EQ),(6,EQ),(7,EQ))                       00027899
//SYSPRINT DD SYSOUT=*                                                  00027999
//SYSIN    DD *                                                         00028099
  SET VAR $PPSPA111        EQ   'STOP'                                  00028199
/*                                                                      00028299
//*                                                                     00028399
//*============================================================         00028499
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS          00028599
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.                      00028699
//*============================================================         00028700
//STOPZEKE EXEC STOPZEKE                                                00028800
//*                                                                     00140000
