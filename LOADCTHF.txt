//LOADCTHF JOB (SYS000),'RENA ',MSGLEVEL=(1,1),
//         CLASS=F,NOTIFY=DEVRWQ,REGION=0M,                             00020005
//         MSGCLASS=X
//************************
//*        RESTART=(STEP4.DSNUPROC),
//************************
//         COND=((8,LT),(5,EQ),(6,EQ),(7,EQ))
//JOBLIB   DD DSN=PPSP.LOADLIB,
//            DISP=SHR
//         DD DSN=DB2P.DCA.LOAD,
//            DISP=SHR
//         DD DSN=DB2P.DSNEXIT,
//            DISP=SHR
//         DD DSN=DB2P.DSNLOAD,
//            DISP=SHR
//*
/*JOBPARM PROCLIB=PROC06
/*ROUTE PRINT RMT1
//*******************************************************************
//*   RELOAD THF TABLES FOR PAYCYCLE PROCESSING
//*   RERUN OF PPP360
//*
//*******************************************************************
//*   THE PERSONNEL TABLE MUST BE LOADED FIRST.
//*   THIS STEP LOADS THE PERSONNEL TABLE
//*******************************************************************
//LOADEDB  EXEC DSNUPROC,
//             SYSTEM=DB2P,
//             UID='C1THF',
//             UTPROC=''
//*
//DSNUPROC.SORTWK01 DD DSN=PPSP.SORTWK01,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//DSNUPROC.SORTWK02 DD DSN=PPSP.SORTWK02,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//DSNUPROC.SORTWK03 DD DSN=PPSP.SORTWK03,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//DSNUPROC.SORTWK04 DD DSN=PPSP.SORTWK04,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SYSUT1: TEMP DATA SET TO HOLD SORTED KEYS IN
//*         "LOAD" AGAINST TABLES WITH INDEXES
//**********************************************
//DSNUPROC.SYSUT1 DD DSN=PPSP.SYSUT1,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SORTOUT: DATA SET TO HOLD SORTED KEYS FOR
//*          POSSIBLE UTILITY RESTART
//**********************************************
//DSNUPROC.SORTOUT DD DSN=PPSP.SORTOUT,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SYSERR: CONTAINS INFORMATION ON ERRORS
//*         ENCOUNTERED DURING UTILITY PROCESSING
//**********************************************
//DSNUPROC.SYSERR DD DSN=PPSP.SYSERR,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SYSMAP: CONTAINS INFORMATION ABOUT WHERE
//*         INPUT RECORDS ARE LOADED
//**********************************************
//DSNUPROC.SYSMAP DD DSN=PPSP.SYSMAP,
//             DISP=(MOD,DELETE,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SYSDISC: CONTAINS ANY INPUT RECORDS WHICH
//*          WERE DISCARDED DURING THE "LOAD"
//**********************************************
//DSNUPROC.SYSDISC DD DSN=PPSP.SYSDISC,
//             DISP=(MOD,CATLG),
//             SPACE=(16384,(20,40),,,ROUND),
//             UNIT=SYSDA
//**********************************************
//* SYSREC: THIS IS THE "LOAD" INPUT DATA SET
//**********************************************
//SYSREC00 DD DSN=PPSP.UDB2THF.INPUT.B10203(PPPPCR),DISP=SHR
//SYSREC01 DD DSN=PPSP.UDB2THF.INPUT.B10203(PPPBSR),DISP=SHR
//SYSREC02 DD DSN=PPSP.UDB2THF.INPUT.B10203(PPPHCR),DISP=SHR
//SYSREC03 DD DSN=PPSP.UDB2THF.INPUT.B10203(PPPTHF),DISP=SHR
//SYSREC04 DD DSN=PPSP.UDB2THF.INPUT.B10203(PPPTTC),DISP=SHR
//SYSREC05 DD DSN=PPSP.UDB2THF.INPUT.B10203(PPPTIM),DISP=SHR
//**********************************************
//* SYSIN: CONTAINS INPUT UTILITY CONTROL
//*         STATEMENTS
//**********************************************
//DSNUPROC.SYSIN  DD DSN=PPSP.UDB2THF.INPUT.B10203($$LOAD),DISP=SHR
//**********************************************************************00480000
