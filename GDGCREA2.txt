//GDGCREAT JOB (SYS000),'BEV 0903',MSGLEVEL=(1,1),                      00010005
//   MSGCLASS=X,CLASS=A,NOTIFY=APCDBM,REGION=4096K                      00030004
//*
//*===================================================================  00003500
//*  CREATING GDG'S
//*===================================================================  00003500
//GDGDEF1  EXEC PGM=IDCAMS                                              00003600
//SYSOUT   DD SYSOUT=*                                                  00003700
//SYSPRINT DD SYSOUT=*                                                  00003800
//SYSIN    DD *                                                         00004600
  DEFINE GDG( -                                                         00041100
   NAME(PPSP.PAY.REDUCT.EXCLFUND.BACKUP) -                              00041100
   LIMIT(13) -
   NOEMPTY -
   SCRATCH)
//*
//*===================================================================  00003500
//GENSUS1  EXEC PGM=IEBGENER                                            03130000
//SYSOUT   DD SYSOUT=*
//SYSPRINT DD SYSOUT=*
//SYSUT1   DD DSN=NULLFILE,
//            DISP=SHR,
//            DCB=(RECFM=FB,LRECL=80,BLKSIZE=27920)
//SYSUT2   DD DSN=PPSP.PAY.REDUCT.EXCLFUND.BACKUP(+1),                  00041100
//            LIKE=SYS1.MODLDSCB,
//            DISP=(NEW,CATLG,DELETE),
//            DCB=(RECFM=FB,LRECL=80),
//            UNIT=SYSDA,SPACE=(TRK,(75,75))
//SYSIN    DD DUMMY
//*
//*================ E N D  O F  J C L  GDGCRE18 =======                 00250027
