//PPSPM717 JOB SYS000,'PRDCNTL',MSGLEVEL=(1,1),
//        NOTIFY=APCDBM,CLASS=A,REGION=0M,
//        MSGCLASS=F
//*++++++++++++++++++++++++++
//*    ZEKE EVENT # 3825
//*++++++++++++++++++++++++++
//**************************************************************        00006044
//*  $PCRCYM - SHOULD REFLECT THE CURRENT MONTHEND YEAR/MONTH  *        00006044
//*           (IE. M1701    FOR JANUARY 2017 (MONTHEND)        *        00006044
//*                                                            *        00006044
//*  RERUN/RESTART INSTRUCTIONS:  JOB CAN BE RERUN FROM THE    *        00006044
//*                               TOP.                         *        00006044
//**************************************************************        00006044
//*====================================================
// JCLLIB ORDER=APCP.PROCLIB                                            00053000
//*====================================================                 00110000
//*---------------------------------------------------------------------
//* STEP01  - PPL717X REVERSE-MAP SOFI FUNDS ON PED FILE
//* STEP02  - PDFMAIL THE OUTPUT CONTROL REPORT AND LOG FILE
//*---------------------------------------------------------------------
//*
//JOBLIB   DD  DSN=PPSP.LOADLIB,DISP=SHR
//         DD  DSN=PPSP.LOADLIB,DISP=SHR
//         DD  DSN=DB2P.DCA.LOAD,DISP=SHR
//         DD  DSN=DB2P.DSNEXIT,DISP=SHR
//         DD  DSN=DB2P.DSNLOAD,DISP=SHR
//         DD  DSN=DB2P.RUNLIB.LOAD,DISP=SHR
//*****************************************************************
//*****************************************************************
//STEP01  EXEC PROC=PPSPM717,
//             PREFIX=PPSP,
//             PRPFX=PPSP.SOFI,
//             MMMYY='$PCRCYM'
//*---------------------------------------------------------------------
//STEP02  EXEC PGM=IKJEFT1B,REGION=0M,DYNAMNBR=50
//STEPLIB  DD  DISP=SHR,DSN=SYS2.TXT2PDF.V3222.LOAD
//         DD  DISP=SHR,DSN=SYS2.XMITIP.V49.LOAD
//SYSEXEC  DD  DISP=SHR,DSN=SYS2.TXT2PDF.V3222.EXEC
//         DD  DISP=SHR,DSN=SYS2.XMITIP.V49.EXEC
//         DD  DISP=SHR,DSN=SISP.PARMLIB
//TXT1     DD  DISP=SHR,DSN=PPSP.PPL717X.CNTLRPT(+1)
//TXT2     DD  DISP=SHR,DSN=PPSP.PPL717X.LOG(+1)
//SYSPRINT DD  SYSOUT=X
//SYSTSPRT DD  SYSOUT=X
//SYSTSIN  DD  *
%MAILER PPL717X - REVERSE-MAP SOFI FUNDS ON PEDF   , Y 0 0
/*
//ADDRLIST DD  *
TO  BFSMAIL-GA-PLANTACCOUNTING@AD.UCSD.EDU
TO  PPSTEAM-L@UCSD.EDU
/*
//*
//*============================================================
//* THE FOLLOWING STEP WILL SET CC 223 IF ANY OF THE JOB STEPS
//* ABEND OR SET AN 'UNACCEPTABLE' CONDITION CODE.
//*============================================================
//STOPZEK4 EXEC STOPZEK4                                                00010000
//*                                                                     00170000
//*================ E N D  O F  J C L  PPSPM717 ========                00250001
